create table blacklist
(
    id         int auto_increment
        primary key,
    type_key   enum ('TOKEN', 'CONNECTION_KEY')   null,
    `key`      text                               not null,
    created_at datetime default CURRENT_TIMESTAMP not null
);

create table genders
(
    gender_id  int auto_increment
        primary key,
    name       varchar(255)                       not null,
    created_at datetime default CURRENT_TIMESTAMP not null,
    updated_at datetime default CURRENT_TIMESTAMP not null
);

INSERT INTO genders (gender_id, name, created_at, updated_at)
VALUES (1, 'Desconhecido', '2021-04-06 00:11:36', '2021-04-06 00:11:36');
INSERT INTO genders (gender_id, name, created_at, updated_at)
VALUES (2, 'Masculino', '2021-04-06 00:11:36', '2021-04-06 00:11:36');
INSERT INTO genders (gender_id, name, created_at, updated_at)
VALUES (3, 'Feminino', '2021-04-06 00:11:36', '2021-04-06 00:11:36');


create table users
(
    user_id        char(36) default ''                not null
        primary key,
    name           varchar(255)                       not null,
    email          varchar(255)                       not null,
    birth_date     date                               not null,
    gender_id      int                                not null,
    password       varchar(255)                       not null,
    login_attempts int      default 0                 not null,
    is_blocked     varchar(255)                       null,
    created_at     datetime default CURRENT_TIMESTAMP not null,
    updated_at     datetime default CURRENT_TIMESTAMP not null,
    constraint users_ibfk_1
        foreign key (gender_id) references genders (gender_id)
);

create index gender_id
    on users (gender_id);

INSERT INTO users (user_id, name, email, birth_date, gender_id, password, login_attempts, is_blocked, created_at, updated_at)
VALUES ('4e2f7925-5623-45f7-a953-22116279dcdb', 'Teste', 'teste@gmail.com', '1997-02-18', 2, '$2a$14$aumvm43d/ruJ6pbqfNpgA.tHbYwBMI.CaI72uEDfpuc5bwgR37YSC', 0, null, '2021-08-07 16:01:49', '2021-08-07 16:01:49');
INSERT INTO users (user_id, name, email, birth_date, gender_id, password, login_attempts, is_blocked, created_at, updated_at)
VALUES ('838c9f15-39bf-4176-876a-1ca4aa58100f', 'Mary Siqueira', 'marrymodaintima@gmail.com', '1980-04-16', 3, '$2a$14$UnUFJn6rSgqHt.ziOrWj9uriciwyET2EEXJxpheENFV8iIjHsn3g2', 0, null, '2021-04-06 00:26:03',
        '2021-04-06 00:26:03');
INSERT INTO users (user_id, name, email, birth_date, gender_id, password, login_attempts, is_blocked, created_at, updated_at)
VALUES ('fe642eaa-55a7-4b82-8ca5-f478bea5ef21', 'Alessandro Vinícius Siqueira Santos', 'alessandro.vncs@gmail.com', '1997-02-18', 2, '$2a$14$UnUFJn6rSgqHt.ziOrWj9uwHnUG/q3dv8Ecyud1/C.i5qX0FY70Dq', 0,
        '2021-08-07T16:17:41.913Z', '2021-04-06 00:23:28', '2021-08-07 15:47:41');


create table user_tags
(
    tag_id     char(36) default ''                not null
        primary key,
    user_id    char(36)                           not null,
    name       varchar(255)                       not null,
    color      varchar(255)                       not null,
    created_at datetime default CURRENT_TIMESTAMP not null,
    updated_at datetime default CURRENT_TIMESTAMP not null,
    constraint user_tags_ibfk_1
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create index user_id
    on user_tags (user_id);

INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('04fea208-56c7-41a6-8810-ad59d5ac94f8', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Boleto loja', '#959595', '2021-04-06 19:31:31', '2021-04-06 19:31:31');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('05494787-6c26-409c-90fe-17cc5e7413fe', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Funerária', '#000c16', '2021-04-06 19:31:00', '2021-04-06 19:31:00');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('1848e834-2647-4999-8e61-aeee95812c70', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Condomínio', '#78c1ff', '2021-04-06 19:32:25', '2021-04-06 21:13:29');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('346caed0-7671-4503-a4a7-a3a1d7558c79', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Cartão de crédito', '#0088ff', '2021-04-06 19:29:27', '2021-04-06 19:29:27');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('392dc70d-83f3-4946-88fa-4ae9b083be9c', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Parcela Bia', '#006ecf', '2021-04-06 19:31:53', '2021-04-06 19:31:53');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('4e4f2b84-7089-4656-b052-306f2ccbf980', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Parcela Celice', '#ff0089', '2021-09-01 12:24:05', '2021-09-01 12:24:05');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('60f0f00b-e6db-431b-9742-acc02fe0d643', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Internet', '#3b89cb', '2021-04-06 20:06:40', '2021-04-06 20:06:40');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('636255a1-444b-4eea-874c-5e9959ed0960', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Plano de saúde', '#004f95', '2021-04-06 19:34:48', '2021-04-06 19:35:04');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('7f02f6fb-b481-4986-a96f-9fe2b526e57f', 'fe642eaa-55a7-4b82-8ca5-f478bea5ef21', 'Cartão Nubank', '#a400ff', '2021-04-06 22:27:39', '2021-04-06 22:27:39');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Empréstimo', '#ff0008', '2021-08-31 14:38:36', '2021-08-31 14:38:36');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('b2bc1523-3985-4f38-9330-cfecb10e1c68', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Academia', '#ff00a2', '2021-08-31 14:45:41', '2021-08-31 14:45:41');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('b8707517-74b3-4495-9ea8-92310be0d37a', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Rastreamento', '#464646', '2021-04-06 19:58:34', '2021-04-06 19:58:34');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('ba35af02-dec8-4cef-81cf-1c66c622899a', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Conta de telefone', '#329af5', '2021-04-06 19:33:34', '2021-04-06 19:33:34');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('d21131e8-fc7b-4e3d-a2cd-bffd7f04751d', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'IPTU', '#97d0ff', '2021-04-06 19:32:51', '2021-04-06 19:32:51');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('d3efabea-544f-4035-aefb-26ae1cb7b398', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Seguro de vida', '#ff1e00', '2021-08-04 19:51:54', '2021-08-04 19:51:54');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('d50b603d-8e93-46c4-8886-ec28efc6f6a9', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'TV assinatura', '#4ba4d5', '2021-04-06 19:30:05', '2021-04-06 19:30:05');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('d541e97e-2f78-4333-b2d2-97316043f944', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Plano dentário', '#376fa1', '2021-04-06 19:35:28', '2021-04-06 19:35:28');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('deec5fb8-d5a2-4c59-8f27-d0788e096d71', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Parcela Márcia bandeira', '#f200ff', '2021-09-01 12:23:43', '2021-09-01 12:23:43');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('f755e9c4-a45f-4737-92f2-77ca82eec738', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Conta de luz', '#557ea3', '2021-04-06 19:29:09', '2021-04-06 19:29:09');
INSERT INTO user_tags (tag_id, user_id, name, color, created_at, updated_at)
VALUES ('fc2365b8-fcc8-4d3c-819f-deaa11e47247', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Boleto carro', '#34a0ff', '2021-04-06 19:34:12', '2021-04-06 19:34:12');


create table user_profiles
(
    profile_id   char(36)   default ''                not null
        primary key,
    user_id      char(36)                             not null,
    name         varchar(255)                         not null,
    main_profile tinyint(1) default 0                 not null,
    created_at   datetime   default CURRENT_TIMESTAMP not null,
    updated_at   datetime   default CURRENT_TIMESTAMP not null,
    constraint user_profiles_ibfk_1
        foreign key (user_id) references users (user_id)
            on delete cascade
);

create index user_id
    on user_profiles (user_id);

INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('03501da3-cdfd-4a38-86e1-2f553d54a825', 'fe642eaa-55a7-4b82-8ca5-f478bea5ef21', 'Padrão', 1, '2021-04-06 00:23:28', '2021-04-06 00:23:28');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('093c001f-70f8-47ef-b920-a9572fdacde6', '4e2f7925-5623-45f7-a953-22116279dcdb', 'meu', 0, '2021-08-07 16:29:42', '2021-08-07 16:29:42');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Marcia Bandeira', 0, '2021-09-01 12:21:17', '2021-09-01 12:21:17');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('26be4d62-7f8b-49c2-a894-728e1c855786', '4e2f7925-5623-45f7-a953-22116279dcdb', 'Padrão', 1, '2021-08-07 16:01:49', '2021-08-07 16:01:49');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('591264e3-9bfd-4614-afff-e6dbe64ab3f9', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Mary', 1, '2021-04-06 00:26:04', '2021-04-06 19:36:29');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('7fc4426a-ef88-48e1-ae56-308db589dee3', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Adamor', 0, '2021-04-06 19:36:08', '2021-04-06 19:36:08');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('887d875b-070d-4606-9fc5-4bb3763f1bb8', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Mary Siqueira', 0, '2021-04-06 20:02:28', '2021-04-06 20:02:28');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('ac9dd2b1-0071-40f9-967e-72d849a05ad0', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Bia', 0, '2021-04-06 19:36:19', '2021-04-06 19:36:19');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('da010c29-c051-4514-95d2-00c371a6adc5', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Heloísa', 0, '2021-07-07 13:31:34', '2021-07-07 13:31:34');
INSERT INTO user_profiles (profile_id, user_id, name, main_profile, created_at, updated_at)
VALUES ('fb2e8aec-a136-4791-bd54-ff1e1a1085a5', '838c9f15-39bf-4176-876a-1ca4aa58100f', 'Celice', 0, '2021-09-01 12:21:04', '2021-09-01 12:21:04');


create table inputs_fixed
(
    input_fixed_id char(36) default ''                not null
        primary key,
    user_id        char(36)                           not null,
    name           varchar(20)                        not null,
    description    varchar(255)                       null,
    value          decimal(10, 2)                     not null,
    tag_id         char(36)                           null,
    created_at     datetime default CURRENT_TIMESTAMP not null,
    updated_at     datetime default CURRENT_TIMESTAMP not null,
    constraint inputs_fixed_ibfk_1
        foreign key (user_id) references users (user_id)
            on delete cascade,
    constraint inputs_fixed_ibfk_2
        foreign key (tag_id) references user_tags (tag_id)
            on delete set null
);

create index tag_id
    on inputs_fixed (tag_id);

create index user_id
    on inputs_fixed (user_id);

create table outputs_fixed
(
    output_fixed_id char(36) default ''                not null
        primary key,
    user_id         char(36)                           not null,
    name            varchar(20)                        not null,
    description     varchar(255)                       null,
    value           decimal(10, 2)                     not null,
    due_day         int unsigned                       null,
    tag_id          char(36)                           null,
    created_at      datetime default CURRENT_TIMESTAMP not null,
    updated_at      datetime default CURRENT_TIMESTAMP not null,
    constraint outputs_fixed_ibfk_1
        foreign key (user_id) references users (user_id)
            on delete cascade,
    constraint outputs_fixed_ibfk_2
        foreign key (tag_id) references user_tags (tag_id)
            on delete set null
);

create index tag_id
    on outputs_fixed (tag_id);

create index user_id
    on outputs_fixed (user_id);

create table movements_input
(
    movement_input_id char(36) default ''                not null
        primary key,
    profile_id        char(36)                           not null,
    name              varchar(25)                        not null,
    description       varchar(255)                       null,
    value             decimal(10, 2)                     not null,
    tag_id            char(36)                           null,
    registration_date date                               not null,
    input_fixed_id    char(36)                           null,
    created_at        datetime default CURRENT_TIMESTAMP not null,
    updated_at        datetime default CURRENT_TIMESTAMP not null,
    constraint movements_input_ibfk_1
        foreign key (profile_id) references user_profiles (profile_id)
            on delete cascade,
    constraint movements_input_ibfk_2
        foreign key (tag_id) references user_tags (tag_id)
            on delete set null,
    constraint movements_input_ibfk_3
        foreign key (input_fixed_id) references inputs_fixed (input_fixed_id)
            on delete set null
);

create index input_fixed_id
    on movements_input (input_fixed_id);

create index profile_id
    on movements_input (profile_id);

create index tag_id
    on movements_input (tag_id);

INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('14eb6fa7-b508-44ec-a553-29825968eeba', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Aposentadoria Adamor ', 'Aposentadoria', 2000.00, null, '2021-05-05', null, '2021-05-05 19:21:46', '2021-05-05 19:22:16');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('17572d3b-9836-4ed0-bc29-d2d52cd94d49', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Bia', 'Pagamento', 655.00, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-08-07', null, '2021-08-07 21:55:50',
        '2021-08-07 21:55:50');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('270b1b79-1389-4b1b-bb3b-f4cf530c527b', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Parcela Bia', 'Pagamento cartão', 116.00, null, '2021-10-01', null, '2021-09-09 00:45:12', '2021-09-09 00:45:12');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('2a3a0013-b665-4fec-a732-b29b1e2b33bf', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Aposentadoria Adamor', 'Banco do Brasil', 2000.00, null, '2021-04-01', null, '2021-04-06 19:50:05', '2021-04-06 19:50:05');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('30516a2d-01b3-4882-b393-e19e73973882', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Adamor', 'Aposentadoria', 3000.00, null, '2021-05-05', null, '2021-05-05 19:09:17', '2021-05-05 19:09:17');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('30f4ba3d-b994-4115-8a72-fcea70b3c0c3', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Bia', 'Pagamento', 349.47, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-04-07', null, '2021-04-07 15:52:34',
        '2021-04-07 15:53:14');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('3461095a-298d-4e96-9285-5bab0a3acfac', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Pagamento do mês', null, 2400.00, null, '2021-09-08', null, '2021-09-09 00:40:12', '2021-09-09 00:40:12');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('4457cb5a-571b-4674-a21b-08fc57e81971', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Pagamento Márcia', 'Celular', 100.00, 'deec5fb8-d5a2-4c59-8f27-d0788e096d71', '2021-09-03', null, '2021-09-03 14:57:58',
        '2021-09-03 14:57:58');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('4e55d163-6d4d-45e4-9cf8-5efc88029005', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Parcela Márcia', 'Pagamento cartão', 100.00, null, '2021-10-01', null, '2021-09-09 00:51:38', '2021-09-09 00:51:38');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('51a841ca-9228-4f2c-998a-08f923023092', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo', 'Pagamento', 5000.00, null, '2021-05-05', null, '2021-05-05 19:31:53', '2021-05-05 19:31:53');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('7555ba29-0af2-440b-8e82-8c7a1ee14952', '26be4d62-7f8b-49c2-a894-728e1c855786', 'salario', 'ew', 123.00, null, '2021-08-07', null, '2021-08-07 16:27:39', '2021-08-07 16:27:39');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('8c527a8f-8075-4f5b-a2d8-fdd7c1e10c77', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Pagamento', null, 9000.00, null, '2021-09-06', null, '2021-09-06 20:54:24', '2021-09-06 20:54:24');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('8ee97df4-9353-4519-88d0-4a729e220912', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Heloísa', 'Pagamento', 200.00, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-08-07', null, '2021-08-07 22:05:31',
        '2021-08-07 22:05:31');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('9377b585-03e8-453c-af39-1840ca837ba1', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Aposentadoria Adamor 2', 'Banco Caixa econômica', 3000.00, null, '2021-04-01', null, '2021-04-06 20:03:34', '2021-04-06 20:03:34');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('af9acd92-3dae-4cfe-91dc-202cad67320a', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Parcela Heloísa', 'Presente Bia', 200.00, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-07-08', null, '2021-07-08 16:03:14',
        '2021-07-08 16:03:14');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('ba901ad8-f7ec-469d-89f6-c6f3037d1d1e', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Parcela Celice', 'Pagamento cartão', 105.00, null, '2021-09-08', null, '2021-09-09 00:49:18', '2021-09-09 00:49:18');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('c1e517a3-07b6-4b7a-a2c3-28e73af4a290', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Bia', 'Parcela mês julho', 460.08, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-07-07', null, '2021-07-07 13:24:24',
        '2021-07-07 13:24:24');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('c3e73d22-d875-4ffb-bf0b-f3d453b9c8e6', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Parcela Bia', 'Bia', 349.47, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-05-06', null, '2021-05-06 13:06:21',
        '2021-05-06 13:07:18');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('c5313aea-03d9-4949-81d7-e44514401782', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Parcela Bia', 'Pagamento cartão', 508.00, null, '2021-09-08', null, '2021-09-09 00:47:17', '2021-09-09 00:47:17');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('ce322b0d-9014-4343-83da-1a5f95f4f3f2', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Pagamento Celice', '1 parcela da rci', 140.00, '4e4f2b84-7089-4656-b052-306f2ccbf980', '2021-09-01', null, '2021-09-01 12:34:42',
        '2021-09-01 12:34:42');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('dd61a72f-9701-4e86-b30a-83a25922f29e', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Aposentadoria Adamor', 'Pagamento mensal', 2000.00, null, '2021-09-06', null, '2021-09-06 20:51:49', '2021-09-06 20:51:49');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('e29268e5-ff77-4fd7-8192-57bb76620406', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Heloísa', 'Parcela Bia', 78.40, '392dc70d-83f3-4946-88fa-4ae9b083be9c', '2021-09-01', null, '2021-08-31 12:54:13',
        '2021-08-31 12:54:13');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('f37866b6-2352-4592-a7a6-1d1c74d8c9d3', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Parcela Bia', 'Pagamento cartão', 85.00, null, '2021-09-08', null, '2021-09-09 00:43:40', '2021-09-09 00:43:40');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('fdbecdf0-9940-4d6f-9405-a4b71f1442ab', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Aposentadoria Adamor', 'Pagamento mensal', 3450.00, null, '2021-09-06', null, '2021-09-06 20:49:01', '2021-09-06 20:49:01');
INSERT INTO movements_input (movement_input_id, profile_id, name, description, value, tag_id, registration_date, input_fixed_id, created_at, updated_at)
VALUES ('fdf48532-3a87-4138-896b-8fb043713f09', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Parcela Bia', 'Pagamento cartão', 115.80, null, '2021-11-01', null, '2021-09-09 00:46:12', '2021-09-09 00:46:12');

create table movements_output
(
    movement_output_id  char(36)     default ''                not null
        primary key,
    profile_id          char(36)                               not null,
    name                varchar(25)                            not null,
    description         varchar(255)                           null,
    value               decimal(10, 2)                         not null,
    current_installment int unsigned default 1                 not null,
    installment_amount  int unsigned default 1                 not null,
    reference_id        char(36)                               not null,
    due_date            date                                   null,
    tag_id              char(36)                               null,
    is_paid             tinyint(1)   default 0                 not null,
    registration_date   date                                   not null,
    output_fixed_id     char(36)                               null,
    created_at          datetime     default CURRENT_TIMESTAMP not null,
    updated_at          datetime     default CURRENT_TIMESTAMP not null,
    constraint movements_output_ibfk_1
        foreign key (profile_id) references user_profiles (profile_id)
            on delete cascade,
    constraint movements_output_ibfk_2
        foreign key (tag_id) references user_tags (tag_id)
            on delete set null,
    constraint movements_output_ibfk_3
        foreign key (output_fixed_id) references outputs_fixed (output_fixed_id)
            on delete set null
);

create index output_fixed_id
    on movements_output (output_fixed_id);

create index profile_id
    on movements_output (profile_id);

create index tag_id
    on movements_output (tag_id);

INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('00ce3ae1-6b6d-42d9-af54-df93ff2ea826', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 59.96, 1, 2, 'd2228511-c4b3-4e2e-afc6-560aced126d3', '2021-04-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1,
        '2021-03-16', null, '2021-04-06 19:45:33', '2021-05-06 13:08:07');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('017c6e0d-4b44-49ce-ac29-dc7b14c2ad97', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Cartão de crédito cred', 'Cartão de crédito', 1202.38, 1, 1, '83b840de-2bf1-442e-8751-4b16837f79ff', '2021-04-13',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:11:01', '2021-04-06 20:11:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0277a9da-7150-47c9-a9ba-42c0361b1dfc', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 8, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-11-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-11-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('02861dab-89d9-4f61-98d2-192ea5408271', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 9, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-05-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2022-04-30', null, '2021-08-31 13:20:59', '2021-09-06 20:50:22');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('033a7d39-ab7c-4bf0-844b-4afdf98bc323', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Itaú', 'Cartão de crédito', 643.72, 1, 1, 'dbed7551-c278-4e44-ab36-915d84041998', '2021-05-15',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-05-05', null, '2021-05-05 19:18:32', '2021-05-05 19:18:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('03acc0b9-8c93-4b03-aa4a-85fe91e21b99', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'TV assinatura claro', 'Claro TV', 388.44, 1, 1, '5d9f446d-f946-4457-a4f4-dfe9bd7b470c', '2021-04-10',
        'd50b603d-8e93-46c4-8886-ec28efc6f6a9', 1, '2021-04-01', null, '2021-04-06 20:04:48', '2021-04-06 20:04:48');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0411d583-b5c4-4f0f-8910-afc327f89bd9', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 9, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-05-15', null, 0,
        '2022-04-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0449ae21-f504-461d-a658-96ff8e5b2077', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Casa contente', 'Fogão', 115.80, 3, 5, 'fb41eb53-911c-4e4e-bb0f-1553c99dab43', '2021-09-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-08', null, '2021-07-08 15:57:28', '2021-09-09 00:44:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('04731272-903a-440f-80d2-c81f5b7b3733', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 204.26, 5, 5, '40f4904f-50c8-42b8-a60a-bb2fa2aee5da', '2021-08-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-08-01', null, '2021-04-06 20:39:43', '2021-05-05 19:33:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('054929ff-ec3c-4ade-8f27-275547c40281', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Boleto Sanny', 'Compra loja', 394.37, 1, 3, 'f3a20a04-a091-4669-96f7-7031bfe72574', '2021-04-26',
        '04fea208-56c7-41a6-8810-ad59d5ac94f8', 0, '2021-03-10', null, '2021-04-06 20:50:23', '2021-05-05 19:34:33');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('05c1b8a4-b5f3-4baa-9226-339314d134a1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 6, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-02-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('05e82fb2-530c-4ab8-a95c-7b20d8f474d3', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 8, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2022-04-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2022-03-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0665ebb1-83d8-40e4-8795-5b6d1961f52f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 33.00, 1, 2, '3e2a5d7e-9d47-4aae-bea5-afd8fc775270', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:59:29', '2021-09-06 20:55:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0788d298-2cf2-496b-b932-f51452ebd967', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Supermercado', null, 88.74, 2, 3, '30f6f967-ff72-44c1-8d6a-87c398669f1e', '2021-06-10', '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0,
        '2021-03-06', null, '2021-04-06 22:35:53', '2021-04-06 22:35:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('08a5f5a8-5869-427f-a453-e842e5a5d6fa', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 6, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-12-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-11-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('08e9477b-83f4-4f12-a1c6-515d03769aab', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 10, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-06-15', null, 0,
        '2022-05-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('093a936d-5f48-477e-ab1a-cc71c3d2356d', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Americana', 'Batedeira', 46.00, 1, 3, '039aaf7d-3a5b-4764-8eb1-1aa3259f3b7e', '2021-07-08', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        1, '2021-06-08', null, '2021-07-08 16:01:10', '2021-09-09 00:44:12');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0be69579-b8be-41f5-8dc7-95a3f82ad7fd', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 4, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:43:55', '2021-09-06 20:57:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0d052cf2-082b-4f79-8f76-5ee2582e93da', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo geru', 'Empréstimo pessoal', 750.00, 4, 4, '8e471716-2785-4700-b4da-51b9b9541797', '2021-12-11',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-11-30', null, '2021-08-31 14:39:53', '2021-09-06 21:02:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0e033e46-228c-4ba2-9613-5611a44dc79a', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Claro TV', 'TV assinatura', 389.00, 1, 1, '237dbeb5-7215-405a-b33b-04023ce90b20', '2021-09-10',
        'd50b603d-8e93-46c4-8886-ec28efc6f6a9', 1, '2021-08-31', null, '2021-08-31 13:18:30', '2021-09-06 20:49:56');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0e7efa9a-5533-4568-b07d-194d52c5ed79', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 3, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0ed77e9c-2a65-4ab3-8c41-161a47e02886', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 10, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-06-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-05-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0f130ea6-7bab-42f8-ae42-dc9fb334f627', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Cartão de crédito', 68.90, 1, 2, '897534ec-7694-463f-8c64-faa286aad886', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-03', null, '2021-04-06 20:18:28', '2021-05-05 19:34:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('0fdae448-dd2c-41c6-9c5e-32d11e5879e6', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 5, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-08-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-08-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('10162a73-678d-4aa0-852c-0d19bf826dcc', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 4, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-07-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-07-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('11252f55-7003-4110-ae03-9188b9a1bd57', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'IPTU', 'IPTU Marcelo', 153.46, 2, 2, '0137edfd-cb63-4af9-83c8-d002226a473f', '2021-05-10', 'd21131e8-fc7b-4e3d-a2cd-bffd7f04751d',
        1, '2021-05-01', null, '2021-04-06 19:48:47', '2021-05-05 19:22:38');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('11cbbef8-41d2-481a-90f3-c6f8819c167f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 16, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-11-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('12cb438f-520f-456d-84b7-3ef5dab9b77c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 18, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2023-02-15', null, 0,
        '2023-01-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('13054c26-f7a2-49de-9678-fc9008ebb8b8', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 8, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('140e6f23-5ccc-47a6-93f0-3abd7ced07e4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 9, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-05-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('15e9b429-139e-4bd8-85e2-990022de21f3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 1, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2021-09-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 1, '2021-08-31', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('15ec9c61-6ba4-4305-93fd-c3760f453450', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Kopcao', 59.50, 1, 1, 'e80d7674-4bc7-4840-940a-e0c71078e14c', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:27:32', '2021-09-06 20:55:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('16c18296-b055-402b-ba4e-a215082dd3b4', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 9, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-05-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-04-30', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('16c98ab0-eaa6-416c-8597-7ea0b3bec91f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 5, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2022-01-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-12-31', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1a3f4822-82c4-4524-b1ce-dd16423ab9d1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Brazz Brazz', 207.84, 1, 2, '2cbdbfc2-d1d8-462d-960c-76d6c7e6ae7c', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-07', null, '2021-04-06 20:19:31', '2021-05-05 19:33:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1b9a0092-dfb1-475d-8413-8bc05da52e5c', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', null, 147.11, 1, 2, 'd128762a-a0bd-4871-aaf9-5f3a793d5334', '2021-07-06', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-06-08', null, '2021-07-06 22:48:24', '2021-07-07 13:24:50');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1bda79a2-5fff-486d-a736-32cee9e66712', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 6, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1bdd21c6-e020-4bc5-b983-5a1541b87621', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Drogasil farmácia', 65.26, 1, 1, 'b0c70ebf-721e-46f2-b045-b63f639b84a5', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-08', null, '2021-04-06 20:24:03', '2021-04-14 20:32:07');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1c47bbf6-693d-4c47-999e-a1aa01bd9f1a', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 5, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-11-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-10-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1c5aaf58-a8da-43d2-877a-d68f4dc3dabe', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 1, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2021-09-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1e098b8b-0d11-47ae-b50f-5f0a045dfd0f', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Conta de luz', 'Luz', 512.06, 1, 1, '43185b59-ffc5-4c24-9119-86a639c0247f', '2021-05-11', 'f755e9c4-a45f-4737-92f2-77ca82eec738',
        1, '2021-05-05', null, '2021-05-05 19:15:10', '2021-05-05 19:15:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1e1c9090-b98b-4589-9ede-5c81c82ac79b', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Internet Net', 'Conta de internet', 192.99, 1, 1, '91749b09-f9c5-4dea-83a3-aac8d6320264', '2021-04-15',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 1, '2021-04-01', null, '2021-04-06 20:06:15', '2021-04-06 20:07:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1e531d21-acb3-4864-aa88-5c5dda436cbf', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 8, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-04-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2022-03-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:40');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1e689d8e-27c9-4ed1-a2bc-d9c549715043', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 7, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1ec04c8e-f84b-44e4-9aa7-6b77cc4d5ccd', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Riachuelo', 'Fatura', 120.00, 2, 2, '8a28467f-4c50-4ee3-b6a0-8394be142409', '2021-10-06', '04fea208-56c7-41a6-8810-ad59d5ac94f8',
        0, '2021-09-30', null, '2021-08-31 13:03:58', '2021-09-06 20:52:04');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1ee21968-5248-40cf-bdfa-7fd41f1deac3', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 10, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-06-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-05-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1f477865-54f8-41e2-bad0-7a816b29769c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 7, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-03-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-02-28', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('1fd6a410-774c-48e8-a133-c65816f5c157', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Casa contente', 'Fogão', 115.80, 4, 5, 'fb41eb53-911c-4e4e-bb0f-1553c99dab43', '2021-10-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-08', null, '2021-07-08 15:57:28', '2021-09-09 00:44:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('2044b827-0086-4533-8e2f-109fb7cb661f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Farmácia', 228.33, 1, 2, '7bcb36f8-2398-44d4-9d12-5f5183647762', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:58:51', '2021-09-06 20:55:48');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('216dc656-4515-443f-86c1-ce7cdb11e6e4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 10, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-06-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2022-05-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('2259a9d4-1b7e-4c72-9562-06de6ed6c932', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 7, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-10-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        0, '2021-09-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('24c7be5c-f443-4026-ac20-d43179f13823', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Mercadinho kiopcao', 64.62, 2, 2, 'b62f2366-3450-47bc-96e8-18b4cdd30041', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-13', null, '2021-04-06 20:25:23', '2021-05-05 19:32:46');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('24cba946-0837-47b7-8a0a-83e071870498', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 95.60, 3, 3, '852537e2-012e-4258-a73e-d7591bcfc307', '2021-06-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0,
        '2021-05-24', null, '2021-04-06 19:44:34', '2021-05-06 13:07:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('2892797d-6a10-4ffa-aadf-de3f7e0a6313', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Compras Mary', 261.54, 3, 4, '65658fd5-8172-4776-a4a5-1134a15b0ec1', '2021-11-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:35:59', '2021-08-31 14:35:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('29de0bab-98ae-4677-8b44-e4eb115cf30a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 204.26, 2, 5, '40f4904f-50c8-42b8-a60a-bb2fa2aee5da', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-05-01', null, '2021-04-06 20:39:43', '2021-05-05 19:33:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('2cc689e2-bfc2-4e05-a24e-5d906382fbc2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 6, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-02-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('2e6403c1-6126-474c-bbe4-4229040c210c', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 6, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-09-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-08-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:19');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('30cd2b36-9848-46e2-9ffe-2d1bd19ef5fb', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Ultrapopular farmácia', 95.73, 1, 2, '1017d26a-fac3-4945-88aa-662576cd4bcd', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-19', null, '2021-04-06 20:46:37', '2021-04-14 20:32:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('31e15774-7727-498c-b58f-aaaa4d1d5a7a', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 12, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-08-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-07-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('320448f9-bb3b-4127-ac30-5ce67a032ab3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 1, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2021-09-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-08-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('321a0696-6957-4777-9947-57d328ec24e7', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura carro', 'Carro', 1496.40, 1, 1, 'be652aee-6176-4c05-9409-2246fd956ebc', '2021-05-19',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 1, '2021-05-05', null, '2021-05-05 19:26:05', '2021-05-05 19:26:05');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('32bde473-b8cc-48f6-b551-39692cb66fa6', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 146.20, 2, 2, '0bb6dec0-809c-4433-81ce-cf4db4cd1905', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 15:00:04', '2021-09-06 20:56:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('339d0c36-1db0-4dd7-be0a-3c8d39337c2d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 2, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:34:20', '2021-09-06 20:59:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('33e779ef-374d-47a8-a2a4-fd17a6e18e79', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 15, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-10-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('34452711-eb93-4266-be7f-c2261e84bb5f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 20, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2023-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2023-03-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3462a0b0-bf01-4caf-b1d9-d75fa6607944', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 10, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-06-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2022-05-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:40');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3495603d-3297-4f9d-9b9c-6383c51ef313', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 2, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2021-10-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3553bab1-ea30-4794-9640-751749b7d87d', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar carro', 'Rastreamento', 59.90, 1, 4, 'a8580d3e-40d8-487d-aa1d-dc8e3c7fea7d', '2021-04-15',
        'b8707517-74b3-4495-9ea8-92310be0d37a', 1, '2021-04-01', null, '2021-04-06 20:00:02', '2021-04-06 20:00:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('35e4434b-025f-4d27-b10c-a715c8caef40', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Supermercado', null, 88.74, 3, 3, '30f6f967-ff72-44c1-8d6a-87c398669f1e', '2021-07-10', '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0,
        '2021-04-06', null, '2021-04-06 22:35:53', '2021-04-06 22:35:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('363325f1-943b-4bce-a54e-f0fd668d9e24', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 11, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2022-02-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2022-02-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('363a8701-d5b8-412c-8b57-7c529d79cc64', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 6, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-09-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-09-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3687b818-8757-4efe-974e-5891bf2e82c4', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 1, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-04-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-04-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('36d0eb78-bee0-41ea-856a-4bab0f0edd5e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo geru', 'Empréstimo pessoal', 750.00, 3, 4, '8e471716-2785-4700-b4da-51b9b9541797', '2021-11-11',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-10-31', null, '2021-08-31 14:39:53', '2021-09-06 21:02:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('38311c28-a819-43a4-ab0e-f7ab41172159', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Jc Ramos', 'Vestuário', 54.00, 2, 2, '05a7c355-1d57-4181-8ecb-51a974ffb44e', '2021-09-07', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-08-07', null, '2021-07-07 13:29:24', '2021-09-09 00:47:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3923d05c-2a40-4e58-b0ef-6be5d07def13', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Alto de pinheiro', 'Condomínio', 300.00, 2, 5, '26c64581-bbaa-4867-8a16-98f8ddaf2c60', '2021-10-10',
        '1848e834-2647-4999-8e61-aeee95812c70', 0, '2021-09-30', null, '2021-08-31 13:24:07', '2021-09-06 20:50:08');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3967fc09-cab5-4212-81b4-c3b166faad37', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Manolito', 95.50, 1, 2, 'f801fe59-0e1c-4d76-a3f8-66c51eea5d5c', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-07', null, '2021-04-06 20:21:40', '2021-05-05 19:33:20');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('39ac977c-e49d-4c59-a034-0bd469ac29b5', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 6, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 13:36:09', '2021-09-06 21:00:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3a106323-42d9-47b8-9d51-7157d62824d6', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 4, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-07-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-07-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3a19634d-b78e-4bdf-b97a-5ee4b31c2d37', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 8, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3b09b879-5cb6-46f8-a05d-8877ee433f95', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Casa contente', 'Fogão', 115.80, 5, 5, 'fb41eb53-911c-4e4e-bb0f-1553c99dab43', '2021-11-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-08', null, '2021-07-08 15:57:28', '2021-09-09 00:44:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3b24e4b8-3370-4f49-86e8-155579683f52', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 2, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2021-10-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2021-09-30', null, '2021-08-31 13:01:01', '2021-09-06 20:52:40');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3c745921-5a60-4c52-af08-9754ce1e98c1', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Alto de pinheiro', 'Condomínio', 300.00, 3, 5, '26c64581-bbaa-4867-8a16-98f8ddaf2c60', '2021-11-10',
        '1848e834-2647-4999-8e61-aeee95812c70', 0, '2021-10-31', null, '2021-08-31 13:24:07', '2021-09-06 20:50:08');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3ce9a4fa-851d-4907-a378-4309b2ec6ebf', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 3, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2021-11-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3d27c5be-dc1d-43b7-9beb-a0108ce8ae7c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Farmácia', 174.77, 1, 1, '264f5356-774b-4371-93d3-0e6185c3ec62', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:33:10', '2021-09-06 20:56:03');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3eae08a9-a5c1-4015-9170-126f7c8f84c7', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 4, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2021-12-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2021-11-30', null, '2021-08-31 12:59:17', '2021-09-06 20:52:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3ef3165c-5836-43d2-be8e-7c863f75fe5c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Manolito', 95.50, 2, 2, 'f801fe59-0e1c-4d76-a3f8-66c51eea5d5c', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-07', null, '2021-04-06 20:21:40', '2021-05-05 19:33:20');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('3f21581a-56f9-47e3-adb8-043489ae24ff', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Sol informática', 54.00, 1, 1, '28c09685-cd4b-4270-9e30-59edeb232e66', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 15:01:22', '2021-09-06 20:56:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('402798cd-1501-4ba7-ab62-331c7365624d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 5, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-01-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('42e476be-e52a-47aa-a582-600421f0bf72', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 146.20, 1, 2, '0bb6dec0-809c-4433-81ce-cf4db4cd1905', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 15:00:04', '2021-09-06 20:56:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('43ddf5e9-0b40-4140-bd6d-5d07e980ba17', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 26.20, 2, 2, 'cee1f2bb-88bd-4539-97c7-21c3d6a5f815', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:56:18', '2021-09-06 20:59:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('44499b05-1982-4ff3-8cee-e819c7c6c587', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 55.33, 1, 2, '05540847-0597-458b-9b2c-22f3fb92227d', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:57:06', '2021-09-06 20:56:42');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4470977e-ce22-431f-91f8-1444eae19019', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 8, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-11-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-11-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('44e432c4-628a-48f2-8ad1-e7407211b50f', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 4, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2021-12-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2021-11-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('45cf77ac-dfdc-498d-8b06-1bf732fa2131', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 3, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-09-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1, '2021-08-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('45fbf32e-c373-4ed5-a197-486bef8addf4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 8, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-04-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2022-03-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4814ef70-d9ba-48c9-83e4-c8de2958442b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 11, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-07-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-06-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('484337f0-976e-4185-a18f-2cfa85e61364', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 4, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('488fc4b4-8e71-40d5-aee7-4badf4ee3b8c', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 6, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-02-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-01-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('48f250c9-f8e6-48b5-922f-79868388aceb', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 5, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4ae03125-a437-4d3c-85b1-cee57dad6e42', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 4, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4bcb545c-eaee-4eb8-a068-d4688fa02fa9', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 10, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2022-01-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2022-01-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4d20a78e-40d4-44ac-ac2a-6694c9c7fa54', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 18, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2023-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2023-01-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4d9dbbe0-41d8-447d-a1fa-edc191d871fb', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 1, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4da261aa-0d1e-4e24-818c-51b0edb95f8c', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 8, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-04-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2022-03-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4e035aa1-ce71-4685-99f8-284731ca44dd', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 7, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('4ec66bfb-1ba1-4c24-80cc-6a302a7308f2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Brazz Brazz', 207.84, 2, 2, '2cbdbfc2-d1d8-462d-960c-76d6c7e6ae7c', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-07', null, '2021-04-06 20:19:31', '2021-05-05 19:33:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('50541bf0-bd3b-40ce-979b-b29d51d04a0f', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 9, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-05-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-04-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('52bc8c62-fd9a-4e61-8a08-857704547a65', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Boleto Sanny', 'Compra loja', 394.37, 2, 3, 'f3a20a04-a091-4669-96f7-7031bfe72574', '2021-05-26',
        '04fea208-56c7-41a6-8810-ad59d5ac94f8', 1, '2021-04-10', null, '2021-04-06 20:50:23', '2021-05-05 19:34:33');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5365975f-c23a-4a82-afe6-85e48368c11d', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 7, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2022-03-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2022-02-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('53ef9283-2e8b-4147-a88a-86ea766902f4', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 3, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-06-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-06-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('54ef6b31-fa15-4f5b-9838-3e573fe12510', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', null, 61.00, 1, 1, '499618e8-913a-4408-a3dc-148ed5eb82f6', '2021-04-09', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        1, '2021-03-08', null, '2021-04-06 20:22:56', '2021-04-14 20:35:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('54f8d2ee-73ac-4761-a664-db52a8d35de1', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 95.60, 2, 3, '852537e2-012e-4258-a73e-d7591bcfc307', '2021-05-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1,
        '2021-04-24', null, '2021-04-06 19:44:34', '2021-05-06 13:07:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('56390e66-db42-4f2a-8c2a-6d4014488936', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 1, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('56b97622-6b2f-4fa7-a73b-20e455506076', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Credcard', 'Cartão de crédito', 279.00, 1, 1, '3daeabd2-0afd-465b-b836-2b9e2bf68885', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 13:24:58', '2021-09-06 20:49:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('56f5fd4e-c7c8-4737-a44b-54514eba4d73', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 5, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-01-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2021-12-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5717ec7e-9472-4e0a-87a0-36a77bd58b78', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão de crédito latam', 'Anuidade cartão', 18.25, 1, 1, 'b9b40060-d993-49aa-9f10-7a6e5af3e6ad', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-28', null, '2021-04-06 20:33:28', '2021-04-14 20:33:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('57194f13-ad71-40de-8068-6dc5efe84f1b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão neo', 'Compras', 429.15, 2, 3, '1b7b0b24-78ae-4aef-8f09-b0296e6953a8', '2021-10-10', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        0, '2021-09-30', null, '2021-08-31 20:15:22', '2021-09-09 00:40:51');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5728ab29-d123-425f-861e-121b74a3b181', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 16, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-12-15', null, 0,
        '2022-11-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5729f676-dff6-4aea-a2c2-299967f85b3c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 6, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-02-15', null, 0,
        '2022-01-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('578041cf-098d-4420-826f-e05659f938d1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 802.50, 1, 1, '210c22b7-3c10-4479-b983-9332b30e493d', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:37:35', '2021-04-14 20:33:38');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5a525097-427e-4377-be2b-1e37e0d91db9', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Bradesco pj', 'Compras', 290.00, 4, 5, '87e21716-29e2-44df-82be-446ded6a5621', '2021-12-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:47:45', '2021-08-31 14:47:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5a7b3c92-2a0c-4166-8223-c6acde867f02', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 5, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5a879dce-dbcd-449d-a97d-c773bd38d1ec', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 33.00, 2, 2, '3e2a5d7e-9d47-4aae-bea5-afd8fc775270', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:59:29', '2021-09-06 20:55:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5b1e7d30-c6c1-488d-b5d8-ae7d178e57de', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 1, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-04-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-04-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5b2b6319-faaa-44ec-a06f-c78c389b7b35', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 3, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5b5a143e-1d14-4815-a3a4-20e5d342d6e8', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Credicard', 'Cartão de crédito', 665.29, 1, 1, '83cefeaf-ba12-41ab-b799-a644b1caf69b', '2021-05-13',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-05-05', null, '2021-05-05 19:16:39', '2021-05-05 19:16:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5dd99e91-30c0-4c71-a97b-70b2aadd23e9', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 6, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-02-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2022-01-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5ebe3a94-de8d-44aa-af8a-d867636ef78b', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Imperador das tintas', 'Material', 86.75, 2, 2, 'd0faf69e-f9be-4312-9dde-64de03fe16bc', '2021-09-08',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1, '2021-08-07', null, '2021-07-07 13:28:05', '2021-09-09 00:48:06');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('5fd9f71b-8c5a-4586-a700-acfe62754580', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 6, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('60eb2cff-b66f-4c67-91e6-8c8bb4454229', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 3, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2021-11-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2021-10-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('61821a48-db11-4c81-8bb8-6447bf3c19fc', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 4, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2021-12-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-11-30', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('61c52b57-b9d1-4c4b-9a12-c53c9366a030', '26be4d62-7f8b-49c2-a894-728e1c855786', 'teste', 'teste', 25.00, 1, 1, 'f924a5da-ffaf-43c8-ad96-694482c9d4d6', null, null, 1, '2021-08-07', null, '2021-08-07 16:26:05',
        '2021-08-07 16:26:05');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('625deefc-aac9-418a-a057-09811ea2f354', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'IPTU', 'IPTU Marcelo', 153.46, 1, 2, '0137edfd-cb63-4af9-83c8-d002226a473f', '2021-04-10', 'd21131e8-fc7b-4e3d-a2cd-bffd7f04751d',
        1, '2021-04-01', null, '2021-04-06 19:48:47', '2021-05-05 19:22:38');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('63079ff7-97b3-4576-bc74-f15468044823', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 1, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2021-09-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        1, '2021-08-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('639fb3af-f870-4baf-9cbb-10248d20f6d8', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 10, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-06-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-05-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('63c77921-8a6a-4855-9ebf-f1486ff1eec7', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 4, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('63fd3172-85c5-4b09-a79e-3e9a0bf73d0b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão de crédito latam', 'SKY pré pago', 113.90, 1, 1, '72dd3d09-afbe-463f-9c6b-04e077fac925', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-24', null, '2021-04-06 20:32:24', '2021-04-14 20:33:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6514514d-7520-4473-938d-17cca9470578', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 9, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-05-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2022-04-30', null, '2021-08-31 12:59:17', '2021-09-06 20:52:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6540b9c4-31f3-4a4b-b13f-d240fc9e408b', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'GM financiamento', 'Carro', 1496.92, 1, 1, 'e7d17870-6a01-46ba-9185-05362470d196', '2021-04-10',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 1, '2021-04-06', null, '2021-04-06 19:55:05', '2021-04-06 19:55:05');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('65b98e6f-074f-43a3-935b-9c77a42ba50b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 3, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2021-11-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2021-10-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('66079636-3733-422b-87ab-4f1f0a4a8702', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão neo', 'Compras', 429.15, 1, 3, '1b7b0b24-78ae-4aef-8f09-b0296e6953a8', '2021-09-10', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        1, '2021-08-31', null, '2021-08-31 20:15:22', '2021-09-09 00:40:51');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('662a4d3c-b020-4890-9815-6736bdfa4d99', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 1, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2021-09-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 1, '2021-08-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('66345e83-ed30-473d-afa3-4bd13274db2d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Cartão de crédito', 405.94, 1, 1, '9d52f06b-8b82-4cf9-a8fb-36cc1b4302f0', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 13:34:32', '2021-09-06 20:57:30');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('664dfb4c-af60-4e4e-bfd4-245e6a1c2d10', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar carro', 'Rastreamento', 59.90, 4, 4, 'a8580d3e-40d8-487d-aa1d-dc8e3c7fea7d', '2021-07-15',
        'b8707517-74b3-4495-9ea8-92310be0d37a', 1, '2021-07-01', null, '2021-04-06 20:00:02', '2021-04-06 20:00:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('66987ae9-8f0d-4f2b-9250-9bd82084a171', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 2, 10, '3c506435-51e7-430e-a693-717d89f31252', '2021-10-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-09-30', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('66b116be-b6bd-4239-9ef1-28b5fa0bbcc4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 6, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('670c9c29-e03d-4f61-9b73-fcc7fe8a1b5b', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Sabonete de Rosto', null, 91.97, 2, 2, 'a8484fef-de27-442c-a65d-1783224c5464', '2021-06-10',
        '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0, '2021-05-06', null, '2021-04-06 22:31:10', '2021-04-06 22:31:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('673fcc9c-97ae-4dec-9b3f-3e8ec491a088', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão neo', 'Compras', 429.15, 3, 3, '1b7b0b24-78ae-4aef-8f09-b0296e6953a8', '2021-11-10', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        0, '2021-10-31', null, '2021-08-31 20:15:22', '2021-09-09 00:40:51');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('69320776-7b59-474e-82f4-b55414866013', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Posto de gasolina', 100.00, 1, 1, '3c4a6f12-33bd-4380-9d02-3fd3293f49b0', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:58:13', '2021-09-06 20:57:44');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6984638a-3889-47e8-bc48-9112a955cb05', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 5, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 13:36:09', '2021-09-06 21:00:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('69867c8b-fb21-409c-9b8a-05feae9f263b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo geru', 'Empréstimo pessoal', 750.00, 1, 4, '8e471716-2785-4700-b4da-51b9b9541797', '2021-09-11',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 1, '2021-08-31', null, '2021-08-31 14:39:53', '2021-09-06 21:02:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6988ca48-53dd-458a-843b-3452c38a0843', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed', 'Plano de saúde', 592.00, 2, 5, '00b80624-1194-4c39-8208-325ca4a19f50', '2021-10-10',
        '636255a1-444b-4eea-874c-5e9959ed0960', 0, '2021-09-30', null, '2021-08-31 13:22:17', '2021-09-06 20:50:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('69de7521-bff3-49e1-86f5-c94d66568504', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 5, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6a49c9fe-1615-402a-94e1-0e69e059259b', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Riachuelo', 'Riachuelo', 9.39, 1, 1, 'ba223863-6034-48d3-9a97-76b6894580b7', '2021-05-05', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        1, '2021-05-05', null, '2021-05-05 19:29:19', '2021-05-05 19:29:19');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6ab489de-9ecf-4e4a-97e6-dc36e410e969', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 4, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-07-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-06-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6ad3af23-4d5b-4090-b34b-1150e08eebf4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Boleto Sanny', 'Compra loja', 394.37, 3, 3, 'f3a20a04-a091-4669-96f7-7031bfe72574', '2021-06-26',
        '04fea208-56c7-41a6-8810-ad59d5ac94f8', 0, '2021-05-10', null, '2021-04-06 20:50:23', '2021-05-05 19:34:33');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6adb408f-d034-43a0-8d98-6b8082a4e144', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Americana', 'Batedeira', 46.00, 2, 3, '039aaf7d-3a5b-4764-8eb1-1aa3259f3b7e', '2021-08-08', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        0, '2021-07-08', null, '2021-07-08 16:01:10', '2021-09-09 00:44:12');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6b0ca797-cc74-43ab-994f-312cc1de9b2f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 3, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 13:36:09', '2021-09-06 21:01:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6c05c3ea-b7dc-4ec8-bd6d-7798fb45aeb9', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Bradesco pj', 'Compras', 290.00, 5, 5, '87e21716-29e2-44df-82be-446ded6a5621', '2022-01-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:47:45', '2021-08-31 14:47:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6c2359e7-9adb-4a7e-a03f-f873b97d220d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 6, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-02-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-01-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6c4e8e2c-98f8-4a48-9bb8-1725eb0e3144', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Bradesco pj', 'Compras', 290.00, 3, 5, '87e21716-29e2-44df-82be-446ded6a5621', '2021-11-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:47:45', '2021-08-31 14:47:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6c62b17b-2f68-45e4-8597-ce9417f2b6ec', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 11, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-07-15', null, 0,
        '2022-06-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6cfe0bfd-8ffb-498e-b288-36247cb61704', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 3, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2021-11-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2021-10-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6d0cb622-b4a5-437f-997a-7bd03ab42a5b', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 3, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2021-11-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2021-10-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6d814f1b-55b1-4afb-8c23-710954ae0d40', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'IntelliJ IDEA', 'Minha IDE', 88.74, 1, 1, '7c306a05-4766-4fc4-a01a-4615230ccb01', '2021-05-10',
        '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0, '2021-04-06', null, '2021-04-06 22:34:24', '2021-04-06 22:34:24');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6e53f32d-1775-4ceb-9af3-5e25e5324689', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Ultrapopular farmácia', 95.73, 2, 2, '1017d26a-fac3-4945-88aa-662576cd4bcd', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-04-19', null, '2021-04-06 20:46:37', '2021-04-14 20:32:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('6e77f9df-064e-4983-9b78-811a17335db5', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 7, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-03-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2022-02-28', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('705c93bb-698e-4f8a-85f2-33b1f73e37b2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 8, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-04-15', null, 0,
        '2022-03-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('70829b53-08c6-4c77-bfe3-5c1125c6e832', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 17, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2023-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-12-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('718a7b3f-f43d-46e6-bed9-343228d05b0b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 2, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('71bea0aa-fd46-481f-8cf4-0c13c8b1354a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 5, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-01-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('72242ee1-48e4-4335-ad2b-3f227ca36be2', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 4, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2021-12-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2021-11-30', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('741e0461-770f-4ccb-937f-bf341dff04d3', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Alto de pinheiro', 'Condomínio', 300.00, 1, 5, '26c64581-bbaa-4867-8a16-98f8ddaf2c60', '2021-09-10',
        '1848e834-2647-4999-8e61-aeee95812c70', 1, '2021-08-31', null, '2021-08-31 13:24:07', '2021-09-06 20:50:08');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7494379a-2c95-43f4-8a7b-28edceb95c38', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Compras Mary', 261.54, 1, 4, '65658fd5-8172-4776-a4a5-1134a15b0ec1', '2021-09-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-08-31', null, '2021-08-31 14:35:59', '2021-08-31 14:35:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75591535-4109-4ec2-ab6c-afde1ad2aa10', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank viajem', 447.51, 1, 3, '1565aa3d-afe9-4822-8af5-95fc20fdec94', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:40:48', '2021-05-05 19:33:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75b88995-50c7-4616-8d08-e8ac79c41d3b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 10, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-06-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-05-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:54');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75d56e69-3950-403d-b41b-f934e59f4494', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 2, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 13:36:09', '2021-09-06 21:01:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75e7386f-ff03-494f-833f-ad65672f4dc4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 1, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75ed5f66-1744-4372-9e12-2f2ec3ab6f29', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Equatorial energia', 'Conta de luz Marcelo', 22.83, 1, 1, 'c689fb9e-d903-4d4a-b9b3-f07a8b433238', '2021-04-11',
        'f755e9c4-a45f-4737-92f2-77ca82eec738', 1, '2021-04-01', null, '2021-04-06 19:53:44', '2021-04-06 19:53:44');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('75f376cf-854d-452e-9c0c-80d87084995b', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Americana', 'Batedeira', 46.00, 3, 3, '039aaf7d-3a5b-4764-8eb1-1aa3259f3b7e', '2021-09-08', '346caed0-7671-4503-a4a7-a3a1d7558c79',
        1, '2021-08-08', null, '2021-07-08 16:01:10', '2021-09-09 00:44:12');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('765d49ec-0174-40b3-b046-b85fdbe3cc43', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 9, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-12-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-12-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('786922a3-2b46-4855-91e9-4c01302e404f', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 4, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2021-12-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-11-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('78bc9dc6-a6c7-4395-bfea-61a8139c42c0', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Equatorial energia', 'Conta de luz', 482.64, 1, 1, '56e0b037-ef10-473a-b143-7f6ad210aade', '2021-04-11',
        'f755e9c4-a45f-4737-92f2-77ca82eec738', 1, '2021-04-01', null, '2021-04-06 20:15:22', '2021-04-06 20:15:22');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('78bce71b-69f3-497e-9be5-5e034515cd9f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Casa contente', 99.80, 3, 3, 'ee302549-fd14-47b8-a366-13abe0a57548', '2021-11-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:36:41', '2021-08-31 14:36:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('791cd94a-38d5-4948-8ed5-63439f12d255', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 1, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7a969469-59a8-4a6b-b3a1-1be159158568', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 10, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-06-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2022-05-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7b1efeb5-232d-4eec-a775-1498a36d580f', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Sabonete de Rosto', null, 91.97, 1, 2, 'a8484fef-de27-442c-a65d-1783224c5464', '2021-05-10',
        '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0, '2021-04-06', null, '2021-04-06 22:31:10', '2021-04-06 22:31:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7b5ed5a1-62af-49d7-bb47-906485485900', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 95.60, 1, 3, '852537e2-012e-4258-a73e-d7591bcfc307', '2021-04-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1,
        '2021-03-24', null, '2021-04-06 19:44:34', '2021-05-06 13:07:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7bba8334-682c-4196-b4f9-eed5b5fbb26d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 3, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7c43e34d-8ba0-4c49-9d53-1a004ff095f5', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Farmácia', 173.09, 2, 2, '0277a9b1-0973-4644-b34c-e9ac22dd6794', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:29:35', '2021-09-06 20:59:27');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7ce4e21e-fe3e-4abb-8b20-e32112d403d1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 12, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-08-15', null, 0,
        '2022-07-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7cf26609-a156-4ed0-86df-5716f2688c63', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Frango assado', 64.00, 1, 1, '66d8e220-4fd0-416b-8fbc-cc86460c2a99', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:57:39', '2021-09-06 20:58:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7d265402-bd3d-4391-b067-ffe62e0e4b72', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 3, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-06-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        0, '2021-05-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7d86366b-d24d-4ff1-965e-5183e2066103', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Líder', 99.30, 1, 1, '613a9ea9-c831-4a68-abcb-b6c76757c6e1', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:25:26', '2021-09-06 20:58:43');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7d911584-2a29-4885-a046-dfc06d8c45c2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 11, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-07-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-06-30', null, '2021-08-31 14:34:20', '2021-09-06 20:59:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('7ff3a504-3bf7-4a90-b045-69855dc3e6b1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Compras Mary', 261.54, 4, 4, '65658fd5-8172-4776-a4a5-1134a15b0ec1', '2021-12-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:35:59', '2021-08-31 14:35:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('80898960-9061-4a23-9240-4e80c570fb47', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 4, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2021-12-15', null, 0,
        '2021-11-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('80b3243a-e861-46a4-9e1e-329631461adb', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 7, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:34:20', '2021-09-06 20:59:10');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('82970de5-3fd3-40f6-8117-8e2b37dba155', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Net', 'Internet', 208.98, 1, 1, '4bfddfea-3b69-4957-a3e5-c052cb3bfeaf', '2021-05-15', '60f0f00b-e6db-431b-9742-acc02fe0d643', 1,
        '2021-05-05', null, '2021-05-05 19:13:06', '2021-05-05 19:13:06');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('83809b8f-1e87-416f-a0c7-41b147557551', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Bradesco pj', 'Compras', 290.00, 1, 5, '87e21716-29e2-44df-82be-446ded6a5621', '2021-09-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-08-31', null, '2021-08-31 14:47:45', '2021-08-31 14:47:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('840ad1a7-6436-4b07-9c75-6a98c69914df', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 15, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-11-15', null, 0,
        '2022-10-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('84222c44-a1d5-4abf-84b0-7945c4ddef2e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 1, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8480fe6f-65cc-46c0-af61-e86ca37a1690', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 4, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2021-12-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2021-11-30', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('85390719-ad43-408e-833c-95547e93d152', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Farmácia', 173.09, 1, 2, '0277a9b1-0973-4644-b34c-e9ac22dd6794', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:29:35', '2021-09-06 20:59:27');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8550a899-8980-423f-8149-d1f708d8bc11', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 4, 10, '3c506435-51e7-430e-a693-717d89f31252', '2021-12-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-11-30', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('85b69c4f-4671-4f26-8a71-f74ac739f144', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 6, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-02-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2022-01-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('85ba6c94-6f35-4c36-a129-e9e495fa4bcf', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 10, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-06-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2022-05-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('86622c53-f6a2-40cd-a763-03eb28aaa88b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 1, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2021-09-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 1, '2021-08-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('869e0cdb-1ae1-40ae-b9f2-87fe3ccd2845', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 4, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2021-12-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8769e5f1-abf1-45ca-8598-0233863ccd40', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 7, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-10-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-10-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8a48cd27-dbc7-403e-858f-a8c053452df8', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 1, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2021-09-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 1, '2021-08-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8af9e25f-b09d-48ff-9c8b-52ab8f2eb608', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 3, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2021-11-15', null, 0,
        '2021-10-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8dd683ba-26cd-490b-ac2d-7e3764292f96', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 1, 10, '3c506435-51e7-430e-a693-717d89f31252', '2021-09-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 1, '2021-08-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8deeb5f1-8d3e-4179-800c-3b91e043e9cb', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 9, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2022-03-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2022-02-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8e0b6496-71f4-494c-9ecf-a0caab24e466', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 2, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('8fd2abf4-899e-4c18-b7e3-aef28c5ac9b8', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 13, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-08-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('90122731-c3cd-43b3-80b0-b71172c65f67', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 26.20, 1, 2, 'cee1f2bb-88bd-4539-97c7-21c3d6a5f815', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:56:18', '2021-09-06 20:59:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9040fe1e-a64d-4847-8143-3a27447e1f49', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 8, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-04-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2022-03-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('90d755a1-fd06-46bf-afbe-68badcb5ea0a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 1, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2021-09-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 1, '2021-08-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('91862252-3af9-4b50-914f-d7d6870643c3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 5, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-01-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2021-12-31', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('91ae09e1-f59a-4be9-968c-040e0390991e', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 7, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2022-01-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-12-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9202070f-3025-414a-ac5d-4b496cb29f67', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed', 'Plano de saúde', 592.00, 3, 5, '00b80624-1194-4c39-8208-325ca4a19f50', '2021-11-10',
        '636255a1-444b-4eea-874c-5e9959ed0960', 0, '2021-10-31', null, '2021-08-31 13:22:17', '2021-09-06 20:50:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('93178a48-e8a5-4dec-85c8-047208690467', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Farmácia', 228.33, 2, 2, '7bcb36f8-2398-44d4-9d12-5f5183647762', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:58:51', '2021-09-06 20:55:48');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('93368a00-a4b8-4c77-8b5b-8da72c3c01cb', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Casa contente', 99.80, 1, 3, 'ee302549-fd14-47b8-a366-13abe0a57548', '2021-09-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-08-31', null, '2021-08-31 14:36:41', '2021-08-31 14:36:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('93996b21-8be8-4256-82f5-7e4ad614fde2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 3, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('945b7020-7f14-447d-98e7-52bbcb89ed6e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 7, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-03-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('945f4d07-1d0c-4cea-90b9-833f7d24c9f3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 2, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2021-10-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('94dc0d9d-f454-4ed0-9dee-a8c04d790ab8', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 1, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2021-09-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 1, '2021-08-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('94f42916-e3f0-4fc4-b72f-9e91f3c57385', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 1, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('94f66b02-98b6-442e-9c30-6fb5129b0e39', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 2, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-08-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-07-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('95edb628-5f1e-4302-aee5-680691afb602', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Conta celular Adamor', 'Claro Adamor', 59.99, 1, 1, 'c6d4738f-ca93-4dec-b67a-3cd6e3750f72', '2021-05-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 1, '2021-05-05', null, '2021-05-05 19:28:31', '2021-05-05 19:28:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('971198a1-311d-46e1-87c7-2abe0044add8', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 6, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-09-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-09-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('97788ec8-1c09-41c3-a5d3-13e15611a666', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 6, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-02-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2022-01-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('97e2433d-ebaa-4702-9b38-f478b2b8ef0b', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Claro celular', 'Celular', 59.99, 1, 1, '48ffd122-bbdd-41b4-a6eb-56a7a7e33cd2', '2021-04-09',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 1, '2021-04-06', null, '2021-04-06 19:57:57', '2021-04-06 19:57:57');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9a872f77-f0db-43b3-bf13-b6bced295af0', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Cartão de crédito', 111.34, 2, 2, 'c8e5a837-4e3c-4cf8-a1f6-5d78c7ff3191', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:23:59', '2021-09-06 21:00:07');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9ca2af5c-a658-4ac0-902f-75e1cff2bab1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Mercadinho kiopcao', 64.62, 1, 2, 'b62f2366-3450-47bc-96e8-18b4cdd30041', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-13', null, '2021-04-06 20:25:23', '2021-05-05 19:32:46');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9cb12d6e-5097-4b3b-bcfd-76a59ac0ecd1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 5, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9d9e06c2-4efc-4cf9-a08d-bace29120fe0', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 6, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2022-02-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-01-31', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9e7b255c-0aed-4266-b5a3-b3157f6f762b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank viajem', 447.51, 2, 3, '1565aa3d-afe9-4822-8af5-95fc20fdec94', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-05-01', null, '2021-04-06 20:40:48', '2021-05-05 19:33:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9f7fc96c-a885-4f1f-a722-b7cb360b75c0', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 7, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-10-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-10-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('9fc54262-3698-4217-be37-729c7b755660', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 5, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-01-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-12-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a0d52cb0-e5d2-4995-ad41-f3c54e09ebf3', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 2, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-05-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-05-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a2193bd9-9724-47f1-894c-1b896ad2432f', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 5, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-01-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-12-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a23071c5-da05-440d-9879-8991d2028915', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 9, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a27a606e-473d-44ad-9de5-bce7f7fa1fc6', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 2, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-05-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-04-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:18');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a28bb160-7541-4a10-bc6e-be1b64dd45dd', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 2, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a37aff3b-fb1e-427b-8c8f-0c417c1bdab6', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 7, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-03-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2022-02-28', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a3d1ca85-2b07-4796-a61d-dd0edfec1fd9', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 10, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-06-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-05-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a3e04b2d-6b8a-4658-a33e-c4707fbf2ac3', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 2, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2021-10-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2021-09-30', null, '2021-08-31 12:59:17', '2021-09-06 20:52:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a435f248-9bd1-4402-9a8c-fda7cbd202dc', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 1, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2021-09-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 1, '2021-08-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a4eb3340-cb36-4ed7-a8cd-febb29deddf1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Cartão de crédito', 68.90, 2, 2, '897534ec-7694-463f-8c64-faa286aad886', '2021-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-03', null, '2021-04-06 20:18:28', '2021-05-05 19:34:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a5c646ad-6f2f-46d2-a046-864b066a6de6', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 12, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-08-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-07-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a60652ee-425f-4670-a946-4372eb52184f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 3, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2021-11-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-10-31', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a6a59889-e56b-4899-abb0-e6198c5970e7', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Plano Unimed', 'Plano de saúde', 592.04, 1, 1, '8b8ef374-343a-4dfc-8722-e0f581827638', '2021-04-20',
        '636255a1-444b-4eea-874c-5e9959ed0960', 1, '2021-04-01', null, '2021-04-06 20:14:11', '2021-04-06 20:14:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a7158d26-862e-434f-a289-db85d52d4792', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Plano de Saúde', null, 392.14, 1, 1, 'cfae9777-ac26-46bb-969b-87159a7e88d4', '2021-05-10', '7f02f6fb-b481-4986-a96f-9fe2b526e57f',
        0, '2021-04-06', null, '2021-04-06 22:30:22', '2021-04-06 22:30:22');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a72027df-7005-4db5-baa0-73b598f1eed0', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed', 'Plano de saúde', 592.00, 4, 5, '00b80624-1194-4c39-8208-325ca4a19f50', '2021-12-10',
        '636255a1-444b-4eea-874c-5e9959ed0960', 0, '2021-11-30', null, '2021-08-31 13:22:17', '2021-09-06 20:50:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a86f0e16-11ef-41a4-917a-2f79d6d13f5f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 19, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2023-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2023-02-28', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a8ac651b-34d4-4156-b0e5-c3d8fe9b726b', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 2, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-05-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-05-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a8ff09b3-fc58-47d4-8eb7-5594980071e4', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 9, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2022-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a93aa1b2-d5db-464d-a6d6-4e36b095a68c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 6, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('a9f6a5a3-5d85-4ca5-9a69-747dd0a42eec', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Claro TV', 'TV assinatura', 388.44, 1, 1, '61fe6463-70bb-4e6d-a355-66815156aebb', '2021-05-10',
        'd50b603d-8e93-46c4-8886-ec28efc6f6a9', 1, '2021-05-05', null, '2021-05-05 19:13:52', '2021-05-05 19:13:52');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('aa97feb1-1700-4038-930f-f552a4137aca', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Máquina de Nota fiscal', 'Aliexpress', 255.73, 1, 1, '46b895b1-84f3-4e9c-b0d5-3d11b77d6c47', '2021-05-10',
        '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0, '2021-04-06', null, '2021-04-06 22:31:48', '2021-04-11 14:45:43');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('aaa8d647-7d57-4681-bfda-958529b77457', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 9, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-05-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-04-30', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ab3c34cf-848d-419c-b6a2-8c314e0b8f23', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 1, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-07-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1, '2021-06-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ab3d9ca6-5108-416d-835a-0a73102a6adf', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 3, 10, '3c506435-51e7-430e-a693-717d89f31252', '2021-11-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-10-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('acb586d2-42c9-4dd1-b99e-e0cceb3a391e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Casa contente', 99.80, 2, 3, 'ee302549-fd14-47b8-a366-13abe0a57548', '2021-10-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:36:41', '2021-08-31 14:36:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ad113b4a-6183-4e9b-96ec-9b7241769990', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Jc Ramos', 'Vestuário', 54.00, 1, 2, '05a7c355-1d57-4181-8ecb-51a974ffb44e', '2021-08-07', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        0, '2021-07-07', null, '2021-07-07 13:29:24', '2021-09-09 00:47:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ad8e255d-0804-4785-b8d8-3e3c62c6972f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank viajem', 447.51, 3, 3, '1565aa3d-afe9-4822-8af5-95fc20fdec94', '2021-06-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-06-01', null, '2021-04-06 20:40:48', '2021-05-05 19:33:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ae21cb8e-ec6a-44f4-b5eb-37ee69219e06', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 14, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-10-15', null, 0,
        '2022-09-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ae2f091b-2a2e-4f8b-83b7-861c58fba4e2', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 5, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-01-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2021-12-31', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('af8f7ecb-58ce-408a-8eda-71250dad34fc', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 8, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('afbf1571-740f-460a-8639-a52a76502ab6', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 7, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-03-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-02-28', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('affeb99c-8fd2-4bfb-8196-49554bf82aa2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 4, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b0181662-263f-4e63-a46b-9bdc4f44309d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Santander', 872.59, 1, 1, 'f3ce37ed-6b0f-4fb4-8135-8275de09547f', '2021-04-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:36:17', '2021-04-14 20:31:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b05f89ca-59dd-406c-bc8c-a75bb2e488e6', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 6, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-02-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-01-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b12fce1f-d7e1-4123-851c-3640577def8e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 2, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2021-10-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2021-09-30', null, '2021-08-31 14:46:39', '2021-09-09 00:41:14');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b1c161bf-f79d-4bd1-9a10-be27461e629a', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed nacional', 'Plano de saúde', 592.04, 1, 1, 'c6ed63a8-bd1e-4bb2-9064-927601ded416', '2021-05-20',
        '636255a1-444b-4eea-874c-5e9959ed0960', 1, '2021-05-05', null, '2021-05-05 19:17:34', '2021-05-05 19:17:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b24768ce-6b11-4fd5-ac67-0d3536ef4f0a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 14, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-09-30', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b2807b24-5f6e-4c47-be59-61e256870b9f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Credicard cartão', 'Avon', 65.00, 1, 1, '0f788c7f-49d8-47d1-99d5-e3dd871cd2d4', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-03-07', null, '2021-04-06 20:20:43', '2021-04-14 20:35:38');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b38e73bc-a18f-444c-8cf7-c81584a6e294', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Caixa Gold', 'Compras Mary', 261.54, 2, 4, '65658fd5-8172-4776-a4a5-1134a15b0ec1', '2021-10-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:35:59', '2021-08-31 14:35:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b459ea1d-3856-4aad-ab69-c2ef3f5564bf', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 11, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-07-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-06-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b4899346-ba8e-4017-9f66-6114520bd9a3', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 6, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-02-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2022-01-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b52dfc4b-b2d2-4bbf-a45e-b736a54c379f', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Adriano variedade', 'Material', 54.00, 1, 2, '8518746a-4231-4f78-a110-699a89877f18', '2021-08-07',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-07-07', null, '2021-07-07 13:26:49', '2021-09-09 00:47:52');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b55208f5-28b9-4ecd-8b66-8e4ec61bee6a', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar carro', 'Rastreamento', 59.90, 2, 4, 'a8580d3e-40d8-487d-aa1d-dc8e3c7fea7d', '2021-05-15',
        'b8707517-74b3-4495-9ea8-92310be0d37a', 1, '2021-05-01', null, '2021-04-06 20:00:02', '2021-04-06 20:00:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b6c503cb-ec5c-4b29-86ac-44f6318db4b9', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 1, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2021-09-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b7118195-87ed-444b-93e6-5ed0dfed58b1', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 9, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2022-05-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2022-04-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b7a1ce5d-5bc5-400b-b1c6-2e21058dc054', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 3, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2021-11-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-10-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b8c53446-8f15-4efa-8aad-62809bb3db37', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 7, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-03-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2022-02-28', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('b9be48be-bd87-425e-83b7-20720583f2be', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 1, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2021-09-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 1, '2021-08-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ba033b14-082c-4447-9f16-d51dbc73c06d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 9, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ba1d0e18-0fb0-4082-9bcd-add43ad60810', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 9, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2022-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bab74fdf-716c-4123-a6f0-7df932f732ec', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 2, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2021-10-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 1, '2021-09-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:56');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bb727e3f-991a-4b75-8b62-942ea3731943', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Equatorial energia', 'Energia elétrica', 650.00, 1, 1, '94b5923d-0e8b-47c6-a4f9-6470deafe7de', '2021-09-10',
        'f755e9c4-a45f-4737-92f2-77ca82eec738', 1, '2021-08-31', null, '2021-08-31 13:20:04', '2021-09-06 20:50:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bbd660bf-d532-44b3-90fc-e0eeed3b2b99', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 5, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-01-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2021-12-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bc089fa1-9ef1-4a8b-ab56-63e76745743e', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 59.96, 2, 2, 'd2228511-c4b3-4e2e-afc6-560aced126d3', '2021-05-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1,
        '2021-04-16', null, '2021-04-06 19:45:33', '2021-05-06 13:08:07');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bc1cc81f-e657-4b21-8c8b-e75beeaa88be', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 3, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bcd36162-10d9-4b04-9b3a-c15e86c3e2a6', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Alto de pinheiro', 'Condomínio', 300.00, 5, 5, '26c64581-bbaa-4867-8a16-98f8ddaf2c60', '2022-01-10',
        '1848e834-2647-4999-8e61-aeee95812c70', 0, '2021-12-31', null, '2021-08-31 13:24:07', '2021-09-06 20:50:08');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('bd2144ce-76a1-4c74-849c-a1a0ad604f8b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 7, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('befc9844-8e08-4baa-9885-42adb027664b', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 4, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2021-10-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-09-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c0b44031-3330-479f-bc60-81c23b3b625b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 8, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2022-04-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c0fe7fee-fedf-4eb0-b5fc-472c08eee4b7', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 4, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c3d3efc9-1b98-4c42-becc-c82e124fbddb', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Riachuelo', 'Fatura', 120.00, 1, 2, '8a28467f-4c50-4ee3-b6a0-8394be142409', '2021-09-06', '04fea208-56c7-41a6-8810-ad59d5ac94f8',
        1, '2021-08-31', null, '2021-08-31 13:03:58', '2021-09-06 20:52:04');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c3fbd6e0-539b-4d1f-9c98-4408edc295be', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 5, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-01-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-12-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c5471cd5-8c98-4ffb-bcf5-36751ce7786c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 5, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-01-15', null, 0,
        '2021-12-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c5b96b71-b78a-4215-b42f-bfbf5a24e3b0', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 10, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-06-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2022-05-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c72ae53c-81d1-4911-bfe7-6652ee485f9f', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Conta luz', 'Luz', 22.53, 1, 1, '5dc308c9-5000-4ba7-8bce-f2ada9f8e7ba', '2021-05-11', 'f755e9c4-a45f-4737-92f2-77ca82eec738', 1,
        '2021-05-05', null, '2021-05-05 19:27:20', '2021-05-05 19:27:20');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c81b8c42-3137-4184-85df-8d5612e52c3a', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 3, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2021-11-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2021-10-31', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c867d0c2-2b7c-431a-bdef-b1d10749ae3a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 14, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-09-30', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('c9327e45-b09b-4939-8ce7-9cf555339e2e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 2, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2021-10-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-09-30', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ca224c53-c8b0-4209-8e89-d6afe18b48c1', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 2, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2021-10-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2021-09-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ca6c61c0-ebbb-4444-ac6a-5fcb7e2301d1', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 10, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2022-04-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2022-03-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cbb3ca0c-b3b3-4548-8457-fddc5344770e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 3, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2021-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cc1bd1e3-62c9-4762-b505-ffa52f007ec4', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 5, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2022-01-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2021-12-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:56');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cccf30d7-1f3c-46a6-89ee-c731e84b121a', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 8, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cd0fa719-239c-4dc1-b788-d4798cb70bac', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 15, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-11-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-10-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cd74f329-141d-4d55-bcac-ab41bbf41e7b', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Imperador das tintas', 'Material', 86.75, 1, 2, 'd0faf69e-f9be-4312-9dde-64de03fe16bc', '2021-08-08',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2021-07-07', null, '2021-07-07 13:28:05', '2021-09-09 00:48:06');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ceeb743b-725c-4ef7-a786-ae586d7c0418', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Adriano variedade', 'Material', 54.00, 2, 2, '8518746a-4231-4f78-a110-699a89877f18', '2021-09-07',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 1, '2021-08-07', null, '2021-07-07 13:26:49', '2021-09-09 00:47:52');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('cf607ed4-cb8e-4e16-b755-4bc223e25b19', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 7, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d0da2889-dfd4-4bc7-980f-f9111af88d76', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Cartão de crédito', 111.34, 1, 2, 'c8e5a837-4e3c-4cf8-a1f6-5d78c7ff3191', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:23:59', '2021-09-06 21:00:07');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d16f1b73-76e8-4e56-90c2-4483a290bbd0', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 12, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2022-03-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2022-03-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d2767535-6f94-4e1a-b2e1-df8097fd97b2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 9, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-05-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d309e855-dadb-4de4-a420-4913c65753c6', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Líder', 124.75, 1, 1, '77d060f9-0c66-4fab-8cec-300f2d609f30', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 14:30:57', '2021-09-06 21:00:22');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d499aebf-6600-4e0a-8a11-43321e32e522', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 9, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2022-05-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2022-04-30', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d49fbcce-a85f-4f5a-b902-116b5f4f0433', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 13, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-09-15', null, 0,
        '2022-08-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d524e6cd-5327-41bb-91a0-c823d0de0c84', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 7, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2022-03-15', null, 0,
        '2022-02-28', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d6b85c73-573d-472f-aa5e-9f965c9631c5', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 5, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d6fe3d15-d15f-4fe3-9858-ed2110279963', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 18.25, 3, 10, 'aca5ec4d-e9b2-4c4e-9694-ddf085c3f77a', '2021-11-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-10-31', null, '2021-08-31 14:22:28', '2021-09-06 21:01:34');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d7be3cb6-f3ab-4854-9712-98c6063ccd58', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 8, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-04-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-03-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d7be6c91-243d-4d89-aaf6-f0a8993b8174', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 3, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-06-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-06-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d7c3310b-9306-46fd-a089-811aa1b1f299', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 204.26, 1, 5, '40f4904f-50c8-42b8-a60a-bb2fa2aee5da', '2021-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:39:43', '2021-05-05 19:33:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d864d62b-6716-4f94-bbcc-038ca590a8b9', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 2, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2021-10-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2021-09-30', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d88a7d82-2581-418c-8445-0811008fbbd0', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 9, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2022-05-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2022-04-30', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d88a8413-fad7-4f8b-802c-8338ad9d9d11', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 1, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-04-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        1, '2021-03-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:19');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d8948d32-e74f-42c7-a8a6-72a2f4ece57d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 4, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2021-12-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2021-11-30', null, '2021-08-31 14:46:39', '2021-09-09 00:41:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d8f723ed-6748-483c-ad61-d81b63a68e97', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed', 'Plano de saúde', 592.00, 1, 5, '00b80624-1194-4c39-8208-325ca4a19f50', '2021-09-10',
        '636255a1-444b-4eea-874c-5e9959ed0960', 1, '2021-08-31', null, '2021-08-31 13:22:17', '2021-09-06 20:50:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d924f8d1-18e6-4b2b-9973-ee0cec7ff6d2', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Boleto carro', 'Carro', 1500.00, 2, 10, '0a1b7ea2-c168-4408-bb61-232a03d797b0', '2021-10-09',
        'fc2365b8-fcc8-4d3c-819f-deaa11e47247', 0, '2021-09-30', null, '2021-08-31 12:57:46', '2021-09-06 20:52:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('d9306781-2bb8-4e99-80ae-582aaa959261', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Cartão Itaú', 'Cartão de crédito', 1022.00, 1, 1, '614d76f8-4a65-4b0b-a5a7-651b7ffca68f', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 13:25:41', '2021-09-06 20:49:39');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('da246928-3c12-4e52-977f-b714176a7d51', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 8, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-04-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-03-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('daf5afa5-4dc4-4c2e-be52-0af2564d1f1b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Bradesco pj', 'Compras', 290.00, 2, 5, '87e21716-29e2-44df-82be-446ded6a5621', '2021-10-10',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:47:45', '2021-08-31 14:47:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('dc1debae-9d9e-4887-908b-683dd28c8bdc', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 953.68, 2, 9, '008b7598-12f1-4af5-836f-f9bb8d9c1aa0', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:43:55', '2021-09-06 20:57:59');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('dc925158-a345-49cc-86aa-658b88b26355', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 4, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2021-12-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('dccc4970-36e6-4931-afa1-a1dc4dd37518', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 8, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ddcd3f2e-03cd-4bba-8ceb-18d16a47200e', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 1, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 13:36:09', '2021-09-06 21:01:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('dede6c00-32b9-40f5-8036-078c2597b85f', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 204.26, 3, 5, '40f4904f-50c8-42b8-a60a-bb2fa2aee5da', '2021-06-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-06-01', null, '2021-04-06 20:39:43', '2021-05-05 19:33:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e0848861-6908-4117-8a32-e40180eb310d', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 6, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e1a9f5d8-5108-4cff-aafe-dcd17bacc0e2', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 7, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-03-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2022-02-28', null, '2021-08-31 12:59:17', '2021-09-06 20:52:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e2143871-c1af-421f-a425-17d8c765892b', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Unimed', 'Plano de saúde', 592.00, 5, 5, '00b80624-1194-4c39-8208-325ca4a19f50', '2022-01-10',
        '636255a1-444b-4eea-874c-5e9959ed0960', 0, '2021-12-31', null, '2021-08-31 13:22:17', '2021-09-06 20:50:45');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e216f11e-a4aa-4ea1-a924-928ae4cf4d56', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Casa contente', 'Fogão', 115.80, 1, 5, 'fb41eb53-911c-4e4e-bb0f-1553c99dab43', '2021-07-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-06-08', null, '2021-07-08 15:57:28', '2021-09-09 00:44:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e28c69eb-cfda-402f-9362-22ada9b16a28', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 8, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2022-04-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2022-03-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e2ba643f-b190-4156-82f0-0360d16918b5', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 149.90, 10, 10, '3c506435-51e7-430e-a693-717d89f31252', '2022-06-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-05-31', null, '2021-09-01 12:29:32', '2021-09-09 00:50:26');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e2cec04f-2d92-4d44-b193-063ab85384b0', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 10, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-06-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-05-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e3773ab6-6914-41c3-9c00-9b9390356e61', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Pneu', 133.40, 9, 9, '2f6484fa-e85c-4086-a74c-77b2b9cd93a7', '2022-05-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-04-30', null, '2021-08-31 14:26:25', '2021-09-06 20:58:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e4bc0cef-cfee-4437-9a68-5fb74ac78b5e', '0f0f7e7e-18c1-48bb-84c5-ee13ab42bfa1', 'Cartão nubank', 'Comprar celular', 100.00, 6, 9, '5535caf6-6ba2-45dc-84b1-7d7aa5585093', '2022-02-09',
        'deec5fb8-d5a2-4c59-8f27-d0788e096d71', 0, '2022-01-03', null, '2021-09-01 12:26:30', '2021-09-09 00:51:56');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e4e10595-2706-42d8-9b9a-a5f0ddace998', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Cartão nubank', null, 193.91, 5, 7, 'f1ad5866-94f2-4c62-b413-ee173b58afb8', '2021-08-09', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        0, '2021-07-24', null, '2021-04-06 19:43:29', '2021-09-09 00:48:19');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e59c9b29-f423-4db2-b797-9c862b7dfc80', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Itaú', 'Cartão de crédito', 120.00, 4, 6, 'da9db4c9-8b8a-4f4b-b6c3-e43b60c06aa8', '2021-12-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-11-30', null, '2021-08-31 13:36:09', '2021-09-06 21:01:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e60566dd-9e7c-4842-91e5-f7f744babebd', 'fb2e8aec-a136-4791-bd54-ff1e1a1085a5', 'Cartão nubank', 'Rci e celular compra', 95.00, 7, 12, 'f1f89a35-053c-433a-a13c-d1a665c713e3', '2022-03-09',
        '4e4f2b84-7089-4656-b052-306f2ccbf980', 0, '2022-02-18', null, '2021-09-01 12:28:12', '2021-09-09 00:50:16');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e65504cd-0dd9-4956-a2a8-7dd36f7350d7', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', null, 147.11, 2, 2, 'd128762a-a0bd-4871-aaf9-5f3a793d5334', '2021-08-06', '392dc70d-83f3-4946-88fa-4ae9b083be9c',
        0, '2021-07-08', null, '2021-07-06 22:48:24', '2021-07-07 13:24:50');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e6ffe14c-34b3-4e7c-8396-9b9a13e9cec2', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Brasil tropical', 300.00, 6, 15, '2271203c-4d82-43dd-b5b5-32f9e1cd3b23', '2022-02-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-01-31', null, '2021-08-31 14:34:20', '2021-09-06 20:59:11');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e7d70389-3804-4220-87d3-81ddeb19f0ba', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 7, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-03-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e8484645-d2fe-47cf-b244-5b59921c8fb5', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Compras', 449.32, 7, 8, '94cd4ba4-f24e-4777-a79d-a96b4278ac93', '2022-03-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-02-28', null, '2021-08-31 14:42:56', '2021-09-06 20:57:01');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e8a7694a-d51c-4693-9a71-81694f2aa2e5', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Uniodonto', 'Plano dentário', 38.52, 5, 9, '6737b881-431d-4bad-9bdb-3609a86d0c04', '2021-08-10',
        'd541e97e-2f78-4333-b2d2-97316043f944', 1, '2021-08-01', null, '2021-04-06 19:52:31', '2021-04-06 19:52:31');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('e9fa923c-ed3c-4292-8205-139c1293ec05', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar', 'GPS carro', 70.00, 4, 10, 'b5c5aabd-e2f6-4e1d-b5c5-87238affa7fb', '2021-12-10', 'b8707517-74b3-4495-9ea8-92310be0d37a',
        0, '2021-11-30', null, '2021-08-31 13:01:01', '2021-09-06 20:52:41');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ec801a6a-f25f-43d4-9ba6-ea756b648da6', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 8, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-04-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2022-03-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ec9f78c3-b86e-4646-aae2-e3b23db648b3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo pessoal', 967.38, 8, 10, 'c81dc761-208f-4111-9efe-539b681c4a77', '2022-04-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-11', null, '2021-09-01 12:19:40', '2021-09-06 20:55:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('eca216e1-c110-4690-84f3-932636f630ab', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 13, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-08-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ed95d1cb-e8e5-469b-92ff-c8b3b1872da3', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Nazaré', 24.19, 1, 1, 'e828d25f-cc59-44b8-a206-2f2c614e4c95', '2021-09-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-08-31', null, '2021-08-31 15:00:45', '2021-09-06 21:01:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('eda40eed-5e76-457c-84bf-1c820fb400ee', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 8, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2022-04-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-03-31', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('efaea9a0-2501-4251-b7d9-815bd3bceea7', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 2, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f0b5a8e3-b195-41c6-a444-fb4699bb1291', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 4, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2021-12-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-11-30', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f1fb5370-8d66-4f69-a191-78bad9ac27ef', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Bruno personal', 'Academia', 700.00, 9, 10, 'dc50906b-2096-4877-9def-31baa7bbe265', '2022-05-10',
        'b2bc1523-3985-4f38-9330-cfecb10e1c68', 0, '2022-04-30', null, '2021-08-31 14:46:39', '2021-09-09 00:41:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f27edd25-4e44-43a3-a336-25048e01731c', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco pj', 'Empréstimo pessoal', 350.00, 3, 10, '13b474bc-5d4e-41b8-93a7-b6f85e08596a', '2021-11-15',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-10-31', null, '2021-08-31 14:48:29', '2021-08-31 14:48:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f2c7d9e2-525d-4534-b3e8-30e328b8b108', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Fatura claro celular', 'Claro celular', 60.00, 6, 10, '7c550b72-191f-4ef0-bf42-b617e9900cd7', '2022-02-08',
        'ba35af02-dec8-4cef-81cf-1c66c622899a', 0, '2022-01-31', null, '2021-08-31 12:59:17', '2021-09-06 20:52:17');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f2f6e4b7-da77-44e7-a9e1-4c17f057e1b6', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'Ótica', 130.00, 5, 9, '2fb803c4-11b8-49f6-9ef5-2b448a6c1a4c', '2022-01-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-12-31', null, '2021-08-31 14:28:40', '2021-09-06 20:57:15');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f3875780-d1f4-4913-8d7d-301eaf074fe5', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 7, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-03-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2022-02-28', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f42dc74c-bdca-424b-bd89-9174e64c613d', '03501da3-cdfd-4a38-86e1-2f553d54a825', 'Supermercado', null, 88.74, 1, 3, '30f6f967-ff72-44c1-8d6a-87c398669f1e', '2021-05-10', '7f02f6fb-b481-4986-a96f-9fe2b526e57f', 0,
        '2021-02-06', null, '2021-04-06 22:35:53', '2021-04-06 22:35:53');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f457671c-c2e0-4699-8e85-18399363ddeb', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Cartão de crédito Itaú', 'Cartão de crédito', 1338.14, 1, 1, 'fd32b32e-d764-4a4d-9774-c53d90521a3e', '2021-04-15',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 1, '2021-04-01', null, '2021-04-06 20:09:21', '2021-04-06 20:09:21');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f565f9d8-976c-43f2-a1bb-030040a8d06b', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo Bradesco', 'Empréstimo pessoal', 2375.00, 2, 6, '383446e3-2974-44b1-8e21-8d32a4c1159f', '2021-10-10',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-09-30', null, '2021-08-31 14:40:40', '2021-09-09 00:40:29');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f73d1d4c-a57c-48e5-b055-175008943b25', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 5, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2022-01-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2021-12-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f745f0cf-8a3b-48f4-8267-88d055ede147', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão nubank', 'Nubank', 204.26, 4, 5, '40f4904f-50c8-42b8-a60a-bb2fa2aee5da', '2021-07-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-07-01', null, '2021-04-06 20:39:43', '2021-05-05 19:33:00');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f74af2a8-3901-44a2-b355-8ecab17a1d54', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão Santander', 'Empréstimo', 1485.20, 12, 20, '6696130c-46b6-4216-976e-2ba8bc67e227', '2022-08-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2022-07-31', null, '2021-08-31 14:45:12', '2021-09-06 20:59:55');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f7bf2fd7-8fd9-458d-a9ec-934a141cbc19', 'ac9dd2b1-0071-40f9-967e-72d849a05ad0', 'Planeta energia', 'Material elétrico', 119.06, 8, 10, '0667d008-d5e2-48b0-a7ae-6ba1b7b5acbe', '2022-02-06',
        '392dc70d-83f3-4946-88fa-4ae9b083be9c', 0, '2022-01-08', null, '2021-07-06 22:49:20', '2021-09-09 00:47:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('f9d00ea4-cb75-4b88-92f7-6ab7dd558ccb', 'da010c29-c051-4514-95d2-00c371a6adc5', 'Casa contente', 'Fogão', 115.80, 2, 5, 'fb41eb53-911c-4e4e-bb0f-1553c99dab43', '2021-08-08',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-07-08', null, '2021-07-08 15:57:28', '2021-09-09 00:44:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fa00485f-4387-4fcd-93be-466d0a9b6b63', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'Alto de pinheiro', 'Condomínio', 300.00, 4, 5, '26c64581-bbaa-4867-8a16-98f8ddaf2c60', '2021-12-10',
        '1848e834-2647-4999-8e61-aeee95812c70', 0, '2021-11-30', null, '2021-08-31 13:24:07', '2021-09-06 20:50:08');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fad06561-4570-4ada-84ec-01fa75f4cf99', '591264e3-9bfd-4614-afff-e6dbe64ab3f9', 'NET', 'Fatura internet', 208.00, 3, 10, '2aeb7824-552b-41f4-8a63-d954f92a41f2', '2021-11-10',
        '60f0f00b-e6db-431b-9742-acc02fe0d643', 0, '2021-10-31', null, '2021-08-31 13:20:59', '2021-09-06 20:50:23');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fb5181d2-eb98-423c-acf3-6673afed8a88', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 17, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2023-01-15', null, 0,
        '2022-12-31', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fbbc91ad-b0c5-491d-b821-8d1fd9b41a47', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo geru', 'Empréstimo pessoal', 750.00, 2, 4, '8e471716-2785-4700-b4da-51b9b9541797', '2021-10-11',
        'a6e0563c-7bf1-4a3a-a72c-6441dc4743ae', 0, '2021-09-30', null, '2021-08-31 14:39:53', '2021-09-06 21:02:32');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fc9e48fa-1501-4cc0-96e2-9c0eb23179a1', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Cartão credcard', 'K opção', 55.33, 2, 2, '05540847-0597-458b-9b2c-22f3fb92227d', '2021-10-09',
        '346caed0-7671-4503-a4a7-a3a1d7558c79', 0, '2021-09-30', null, '2021-08-31 14:57:06', '2021-09-06 20:56:42');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fd2921f6-019d-45e9-8e8c-4e2e402d3c4a', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Onstar carro', 'Rastreamento', 59.90, 3, 4, 'a8580d3e-40d8-487d-aa1d-dc8e3c7fea7d', '2021-06-15',
        'b8707517-74b3-4495-9ea8-92310be0d37a', 1, '2021-06-01', null, '2021-04-06 20:00:02', '2021-04-06 20:00:02');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('fdc8eefe-9f80-4fed-99c3-16db7ee6d983', '887d875b-070d-4606-9fc5-4bb3763f1bb8', 'Empréstimo caixa', 'Empréstimo pessoal', 850.00, 2, 18, 'dc8ebde6-2348-4a01-bdc1-8b99a75f3ff1', '2021-10-15', null, 0,
        '2021-09-30', null, '2021-08-31 14:38:08', '2021-09-06 21:02:58');
INSERT INTO movements_output (movement_output_id, profile_id, name, description, value, current_installment, installment_amount, reference_id, due_date, tag_id, is_paid, registration_date, output_fixed_id, created_at,
                              updated_at)
VALUES ('ff2e4b4a-2ae6-4787-ae50-ae1fddf45a19', '7fc4426a-ef88-48e1-ae56-308db589dee3', 'Parque das palmeiras', 'Funerária', 43.82, 9, 12, '71aeef71-5a94-4393-8d20-48e1b016dd01', '2021-12-10',
        '05494787-6c26-409c-90fe-17cc5e7413fe', 1, '2021-12-01', null, '2021-04-06 19:56:39', '2021-04-06 19:56:39');


