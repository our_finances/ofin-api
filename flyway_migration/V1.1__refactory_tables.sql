create table movements
(
    movement_id         char(36)         not null,
    movement_type       varchar(12)      not null, -- INCOME :: OUTCOME
    profile_id          char(36)         not null,
    name                varchar(25)      not null,
    description         varchar(255),
    value               decimal(10, 2)   not null,
    current_installment int(10) unsigned not null default 1,
    installment_amount  int(10) unsigned not null default 1,
    reference_id        char(36),
    tag_id              char(36)                  default null,
    due_date            date                      default null,
    is_paid             tinyint(1)                default 0,
    registration_date   date             not null,
    created_at          datetime         not null default CURRENT_TIMESTAMP,
    updated_at          datetime         not null default CURRENT_TIMESTAMP,
    constraint pk_movements primary key (movement_id),
    constraint fk_movements_tag_id foreign key (tag_id) references user_tags (tag_id) on delete set null,
    constraint fk_movements_profile_id foreign key (profile_id) references user_profiles (profile_id)
);

create table profile_balance_monthly
(
    balance_id    char(36)       not null,
    profile_id    char(36)       not null,
    total_income  decimal(19, 2) not null,
    total_outcome decimal(19, 2) not null,
    month         int unsigned   not null,
    year          int unsigned   not null,
    created_at    datetime       not null default CURRENT_TIMESTAMP,
    updated_at    datetime       not null default CURRENT_TIMESTAMP,
    constraint pk_profile_balance_monthly primary key (balance_id),
    constraint fk_profile_balance_monthly_profile_id foreign key (profile_id) references user_profiles (profile_id)
);

create table movement_fixed
(
    movement_fixed_id char(36)       not null,
    profile_id        char(36)       not null,
    tag_id            char(36),
    movement_type     varchar(12)    not null,
    name              varchar(25)    not null,
    description       varchar(255),
    value             decimal(19, 2) not null,
    due_day           int unsigned,
    created_at        datetime       not null default CURRENT_TIMESTAMP,
    updated_at        datetime       not null default CURRENT_TIMESTAMP,
    constraint pk_movement_fixed primary key (movement_fixed_id),
    constraint fk_movement_fixed_profile_id foreign key (profile_id) references user_profiles (profile_id),
    constraint fk_movement_fixed_tag_id foreign key (tag_id) references user_tags (tag_id)
);


insert into movements (movement_id, movement_type, profile_id, name, description, value, current_installment, installment_amount, reference_id, tag_id, due_date, is_paid, registration_date, created_at, updated_at)
select mi.movement_input_id,
       'INCOME',
       mi.profile_id,
       mi.name,
       mi.description,
       mi.value,
       1,
       1,
       null,
       mi.tag_id,
       null,
       0,
       mi.registration_date,
       mi.created_at,
       mi.updated_at
from movements_input as mi
union all
select mo.movement_output_id,
       'OUTCOME',
       mo.profile_id,
       mo.name,
       mo.description,
       mo.value,
       mo.current_installment,
       mo.installment_amount,
       mo.reference_id,
       mo.tag_id,
       mo.due_date,
       mo.is_paid,
       mo.registration_date,
       mo.created_at,
       mo.updated_at
from movements_output as mo;


drop table if exists movement_temp;
create temporary table movement_temp
(
    movement_type varchar(10),
    profile_id    char(36),
    value         decimal(10, 2),
    month         int,
    year          int
);

insert into movement_temp (movement_type, profile_id, value, month, year)
select 'OUTCOME', mo.profile_id, sum(mo.value), month(mo.registration_date), year(mo.registration_date)
from movements_output as mo
group by mo.profile_id, month(mo.registration_date), year(mo.registration_date);


insert into movement_temp (movement_type, profile_id, value, month, year)
select 'INCOME', mi.profile_id, sum(mi.value), month(mi.registration_date), year(mi.registration_date)
from movements_input as mi
group by mi.profile_id, month(mi.registration_date), year(mi.registration_date);

CREATE FUNCTION uuid_v4()
    RETURNS CHAR(36) no sql
BEGIN
    SET @h1 = HEX(RANDOM_BYTES(4));
    SET @h2 = HEX(RANDOM_BYTES(2));
    SET @h3 = SUBSTR(HEX(RANDOM_BYTES(2)), 2, 3);
    SET @h4 = CONCAT(HEX(FLOOR(ASCII(RANDOM_BYTES(1)) / 64)+8),
                     SUBSTR(HEX(RANDOM_BYTES(2)), 2, 3));
    SET @h5 = HEX(RANDOM_BYTES(6));
    RETURN LOWER(CONCAT(
            @h1, '-', @h2, '-4', @h3, '-', @h4, '-', @h5
        ));
END;

insert into profile_balance_monthly (balance_id, profile_id, total_income, total_outcome, month, year)
select uuid_v4(), mv.profile_id, sum(if(mv.movement_type = 'INCOME', mv.value, 0)) as total_income, sum(if(mv.movement_type = 'OUTCOME', mv.value, 0)) as total_outcome, mv.month as month, mv.year as year from movement_temp as mv
where mv.month <= month(now()) and mv.year <= year(now())
group by mv.profile_id, mv.month, mv.year
order by mv.year, mv.month;

drop function uuid_v4;
drop table if exists movement_temp;
drop table movements_input;
drop table movements_output;
drop table inputs_fixed;
drop table outputs_fixed;

rename table blacklist to token_blacklist;
