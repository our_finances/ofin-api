import UserBuilder from '../builder/user-builder'
import UserService from '../../src/app/services/user-service'
import { UserInstance } from '../../src/app/models/user-model'
import Api from './api'

export default class InitSystem {
  private userSrv = new UserService()

  constructor(private api: Api) {
    this.api = api
  }

  async createRandomPlayer(): Promise<UserInstance> {
    const user = new UserBuilder().build()
    return await this.userSrv.createUser(user)
  }
}
