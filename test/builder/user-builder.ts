import { CreateUserInterface } from '../../typings/user'
import { GenderEnum } from '@enums/gender-enum'
import * as faker from 'faker'

class UserBuilder {
  private readonly user: CreateUserInterface

  constructor() {
    this.user = {
      name: faker.name.findName(),
      genderId: GenderEnum.MASCULINO,
      password: faker.internet.password(6),
      birthDate: faker.date.past(1997).toISOString(),
      email: faker.internet.email()
    }
  }

  invalidEmail(): UserBuilder {
    this.user.email = faker.lorem.words(2)
    return this
  }

  invalidBirthDate(): UserBuilder {
    this.user.birthDate = faker.lorem.words(3)
    return this
  }

  build(): CreateUserInterface {
    return this.user
  }
}

export default UserBuilder
