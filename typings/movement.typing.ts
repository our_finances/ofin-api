import { MovementTypeEnum } from '@enums/movement-type.enum'
import { Null } from './generic.typing'

export interface MovementResponseDTO {
  movementId: string
  movementType: MovementTypeEnum
  name: string
  description: string
  value: number
  currentInstallment: number
  installmentAmount: number
  referenceId: Null<string>
  tag: {
    tagId: string
    name: string
    color: string
  }
  dueDate: Date
  isPaid: boolean
  registrationDate: Date
}

export interface CreateMovementOutcomeRequestDTO {
  name: string
  description: Null<string>
  value: number
  installments: number
  dueDate: Null<Date>
  registrationDate: Null<Date>
  isPaid: boolean
  tagId: Null<string>
}

export interface EditMovementOutcomeRequestDTO {
  movementId: string
  name?: string
  description?: Null<string>
  value?: number
  tagId?: Null<string>
  isPaid?: boolean
}

export interface CreateMovementIncomeRequestDTO {
  name: string
  description: Null<string>
  value: number
  tagId: Null<string>
  registrationDate: Null<Date>
}

export interface EditMovementIncomeRequestDTO {
  movementId: string
  name?: string
  description?: Null<string>
  value?: number
  tagId?: Null<string>
}

export interface RawSumMovementOfCurrentMonth {
  profileId: string
  totalIncome: string
  totalOutcome: string
}

export interface SumMovementOfCurrentMonth {
  profileId: string
  totalIncome: number
  totalOutcome: number
}
