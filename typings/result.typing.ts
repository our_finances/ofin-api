import { messages } from '@utils/translate'

export const statusCode = {
  OK: 200,
  Created: 201,
  Accepted: 202,
  'No Content': 204,
  'Bad Request': 400,
  Unauthorized: 401,
  Forbidden: 403,
  'Not Found': 404,
  'Unprocessable Entity': 422,
  'Internal Server Error': 500,
  'Service Unavailable': 503
}

export const getStatusByCode = (statusNumber: number) => {
  for (const [key, value] of Object.entries(statusCode)) {
    if (value === statusNumber) {
      return statusCode[key as keyof typeof statusCode]
    }
  }
  return statusCode['Internal Server Error']
}

export interface ResultData {
  status: keyof typeof statusCode
  msg: messages
  body?: unknown
  connectionKey?: string
  error?: unknown
  token?: string
  refreshToken?: string
}
