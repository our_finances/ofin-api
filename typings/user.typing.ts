export interface CreateUserRequestDTO {
  name: string
  email: string
  genderId: number
  password: string
  birthDate: number
}

export type EditUserRequestDTO = {
  userId: string
} & Omit<Partial<CreateUserRequestDTO>, 'password' | 'email'>

export interface ChangePasswordRequestDTO {
  userId: string
  currentPassword: string
  newPassword: string
  confirmPassword: string
  isRecoveryPassword: boolean
}
