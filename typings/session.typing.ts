import { User } from '@modules/user/user.entity'

export interface SessionResponseDTO {
  data: User
  token?: string
  refreshToken?: string
  connectionKey?: string
}

export interface UserSessionRequestDTO {
  email: string
  password: string
  stayConnected: boolean
}
