export interface CreateUserProfileRequestDTO {
  name: string
}

export interface UpdateUserProfileRequestDTO {
  profileId: string
  name?: string
}

export interface DeleteUserProfileRequestDTO {
  profileId: string
}

export interface ProfileResponseDTO {
  profileId: string
  name: string
}

export interface ProfileWithBalance {
  profileId: string
  name: string
  mainProfile: string
  totalIncome: number
  totalOutcome: number
}

export interface RawProfileWithBalance {
  profileId: string
  name: string
  mainProfile: string
  totalIncome: string
  totalOutcome: string
}
