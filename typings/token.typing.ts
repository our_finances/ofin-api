export interface GenerateToken {
  token: string
  refreshToken: string
}

export interface GenerateKeyConnection {
  connectionKey: string
}

export interface JwtObj {
  key: string
  iat?: number
  exp?: number
}

export interface TokenArray {
  id: string
  token?: string
}

export type CheckTokenResponse = {
  id: string
} & Partial<GenerateToken> &
  Partial<GenerateKeyConnection>

export interface CheckKeyConnectionResponse {
  id: string
  connectionKey: string
}
