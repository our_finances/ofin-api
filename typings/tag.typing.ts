export interface CreateTagRequestDTO {
  name: string
  color: string
}

export type EditTagRequestDTO = {
  tagId: string
} & Partial<CreateTagRequestDTO>

export interface DeleteTagRequestDTO {
  tagId: string
}

export interface TagResponseDTO {
  tagId: string
  name: string
  color: string
}
