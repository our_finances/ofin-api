import { messages } from '@utils/translate'
import { HttpStatus } from '@nestjs/common'

class BaseError extends Error {
  constructor(message: string) {
    super(message)
    this.name = new.target.name
    Object.setPrototypeOf(this, new.target.prototype)
    Error.captureStackTrace(this, new.target)
  }
}

export class ApiErrorException extends BaseError {
  public status = HttpStatus.INTERNAL_SERVER_ERROR
  public readonly description: messages
  public readonly body: unknown

  constructor(status: HttpStatus, description: messages, body?: unknown) {
    super(HttpStatus[status])
    this.status = status
    this.description = description
    this.body = body
  }
}
