import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  NotFoundException
} from '@nestjs/common'
import { WinstonLoggerService } from '@logger/winston-logger.service'
import { ResponseApi } from '@utils/result.util'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { Response } from 'express'
import { Null } from '@typings/generic.typing'
import { ZodError, ZodIssue } from 'zod'
import { exceptions, messages } from '@utils/translate'

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private logger = new WinstonLoggerService()
  private response: Null<Response> = null

  constructor() {
    this.logger.setContext(AllExceptionsFilter.name)
  }

  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse()
    const request = ctx.getRequest()

    this.response = response

    this.logger.error(exception.name)
    this.logger.error(exception.stack)

    const exceptionList: Record<string, (ex: any) => void> = {
      ApiErrorException: this.apiErrorException,
      ZodError: this.zodValidationException,
      NotFoundException: this.notFoundException
    }

    const execFuncException = exceptionList[exception.name] ?? null

    if (execFuncException) {
      return execFuncException.bind(this)(exception)
    }

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR

    const res = new ResponseApi(response)
      .setStatus(status)
      .setCode('unknownError')
      .setBody({ path: request.url })

    return res.send()
  }

  private apiErrorException(exception: ApiErrorException) {
    this.logger.error(
      `${exception.description} - {}`,
      messages[exception.description]
    )
    const response = new ResponseApi(this.getResponse())
    return response
      .setStatus(exception.status)
      .setCode(exception.description)
      .setBody(exception.body)
      .send()
  }

  private zodValidationException(exception: ZodError) {
    const response = new ResponseApi(this.getResponse())

    this.logger.error(`Error na validação do zod: ${JSON.stringify(exception)}`)
    const allErrors = exception.issues.map((err) => {
      const replaceFields = []
      switch (err.code) {
        case 'invalid_type': {
          replaceFields.push(err.expected)
          return this.zodErrorBuild('invalid_type', replaceFields, err)
        }

        case 'too_big': {
          replaceFields.push(err.maximum)
          return this.zodErrorBuild('max', replaceFields, err)
        }

        default: {
          return {
            field: err.path.join('.'),
            error: err.message
          }
        }
      }
    })

    return response
      .setStatus(HttpStatus.UNPROCESSABLE_ENTITY)
      .setCode('validationError')
      .setBody(allErrors)
      .send()
  }

  private notFoundException(exception: NotFoundException) {
    const response = new ResponseApi(this.getResponse())
    return response
      .setStatus(HttpStatus.NOT_FOUND)
      .setCode('routeNotFound')
      .send()
  }

  private zodErrorBuild(
    exception: keyof typeof exceptions,
    values: (string | number)[],
    zodIssue: ZodIssue
  ) {
    let text = exceptions[exception]
    values.forEach((rf) => {
      text = text.replace('%', rf.toString())
    })
    return {
      field: zodIssue.path.join('.'),
      error: text
    }
  }

  private getResponse() {
    if (this.response) {
      return this.response
    }
    throw new Error('Sem response')
  }
}
