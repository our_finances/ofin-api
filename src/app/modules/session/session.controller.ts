import { Controller, Post, Req, Res } from '@nestjs/common'
import { SessionService } from '@modules/session/session.service'
import { Request, Response } from 'express'
import * as zod from 'zod'
import { ResponseApi } from '@utils/result.util'

@Controller('sessions')
export class SessionController {
  constructor(private sessionService: SessionService) {}

  @Post('login')
  async createSessionUser(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)
    const schemaSessionUser = zod.object({
      email: zod.string(),
      password: zod.string(),
      stayConnected: zod.boolean()
    })

    const body = schemaSessionUser.parse(req.body)
    const { token, refreshToken, connectionKey } =
      await this.sessionService.sessionUser(body)

    response
      .setOk('userLoginSuccess')
      .setToken(token)
      .setRefreshToken(refreshToken)
      .setConnectionKey(connectionKey)

    return response.send()
  }

  @Post('logout')
  async logoutSessionUser(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)
    const user = res.locals.user

    await this.sessionService.logoutSessionUser(user.userId, res)

    return response.setOk('userLogoutSuccess').send()
  }
}
