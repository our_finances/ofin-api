import { Module } from '@nestjs/common'
import { SessionController } from '@modules/session/session.controller'
import { SessionService } from '@modules/session/session.service'
import { UserModule } from '@modules/user/user.module'
import { AuthenticationModule } from '@modules/authentication/authentication.module'

@Module({
  controllers: [SessionController],
  providers: [SessionService],
  imports: [UserModule, AuthenticationModule]
})
export class SessionModule {}
