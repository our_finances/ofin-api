import { HttpStatus, Injectable } from '@nestjs/common'
import { Response } from 'express'
import { messages } from '@utils/translate'
import { addMinutes, differenceInMinutes } from 'date-fns'
import {
  SessionResponseDTO,
  UserSessionRequestDTO
} from '@typings/session.typing'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { UserService } from '@modules/user/user.service'
import CryptUtil from '@utils/crypt.util'
import { AuthenticationService } from '@modules/authentication/authentication.service'
import { User } from '@modules/user/user.entity'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
export class SessionService {
  private cryptUtil: CryptUtil

  constructor(
    private logger: LoggerAbstract,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    this.logger.setContext(SessionService.name)
    this.cryptUtil = new CryptUtil()
  }

  async sessionUser(data: UserSessionRequestDTO): Promise<SessionResponseDTO> {
    this.logger.info('Iniciando sessão de usuário')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando usuário pelo email: ${data.email}`)
    const sessionUser = await this.userService.findOneByEmail(data.email)

    if (!sessionUser) {
      this.logger.error('Usuário não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userNotFound')
    }

    await this.checkUserBlocked(sessionUser)

    if (!this.cryptUtil.compareHash(data.password, sessionUser.password)) {
      await this.checkLoginAttempts(sessionUser)
    } else {
      sessionUser.loginAttempts = 0
      await this.userService.saveUser(sessionUser)
    }

    if (data.stayConnected) {
      this.logger.info('Gerando connectionKey')
      return {
        data: sessionUser,
        connectionKey: this.authenticationService.generateUserKeyConnection(
          sessionUser.userId
        )
      }
    }

    this.logger.info('Gerando tokens')
    const { token, refreshToken } =
      this.authenticationService.generateUserToken(sessionUser.userId)

    return {
      data: sessionUser,
      token,
      refreshToken
    }
  }

  async logoutSessionUser(userId: string, res: Response): Promise<void> {
    try {
      this.logger.info(`Fazendo Logout do usuário: ${userId}`)

      if (res.locals.connectionKey) {
        this.logger.info('Revogando connectionKey')
        await this.authenticationService.revokeConnectionKey(
          res.locals.connectionKey
        )
      } else {
        this.logger.info('Revogando token...')
        await this.authenticationService.revokeToken(res.locals.token)
      }

      this.logger.info('Limpando token e connectionKey')
      res.locals.token = undefined
      res.locals.refreshToken = undefined
      res.locals.connectionKey = undefined
    } catch (e) {
      this.logger.error(`Error ao fazer logout do usuário: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userLogoutError'
      )
    }
  }

  private async checkLoginAttempts(sessionUser: User): Promise<void> {
    this.logger.info('Validando senha do usuário')
    const attempts = sessionUser.loginAttempts + 1
    sessionUser.loginAttempts = attempts
    let msg: messages = 'userPasswordNotEqual'
    if (attempts > 3) {
      sessionUser.loginAttempts = 0
      sessionUser.isBlocked = addMinutes(new Date(), 30)
      msg = 'userBlocked'
      this.logger.info('Usuário foi bloqueado.')
    }
    await this.userService.saveUser(sessionUser)
    this.logger.error('A senha do usuário não corresponde')
    throw new ApiErrorException(HttpStatus.BAD_REQUEST, msg)
  }

  private async checkUserBlocked(sessionUser: User): Promise<void> {
    this.logger.info('Verificando se usuário está bloqueado')
    if (!sessionUser.isBlocked) {
      return
    }

    const timeRemains = differenceInMinutes(
      new Date(sessionUser.isBlocked),
      new Date()
    )

    const msg = `Bloqueado por ${timeRemains} minutos`
    this.logger.info(msg)
    if (timeRemains > 0) {
      this.logger.info('Usuário ainda bloqueado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userBlocked', msg)
    }

    this.logger.info('Usuário desbloqueado')
    sessionUser.isBlocked = null
    await this.userService.saveUser(sessionUser)
  }
}
