import {
  CheckKeyConnectionResponse,
  CheckTokenResponse,
  GenerateToken
} from '@typings/token.typing'
import CryptUtils from '@utils/crypt.util'
import TokenUtil from '@utils/token.util'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { InjectRepository } from '@nestjs/typeorm'
import { TokenBlacklist } from '@modules/authentication/token-blacklist.entity'
import { Repository } from 'typeorm'
import { TokenKeyEnum } from '@enums/token-key.enum'
import { Undefined } from '@typings/generic.typing'
import { HttpStatus } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'

export class AuthenticationService {
  private tokenUtil = new TokenUtil()
  private cryptUtil = new CryptUtils()

  constructor(
    private logger: LoggerAbstract,
    @InjectRepository(TokenBlacklist)
    private repository: Repository<TokenBlacklist>
  ) {
    this.logger.setContext(AuthenticationService.name)
  }

  async checkToken(
    token: string,
    refreshToken: string
  ): Promise<CheckTokenResponse> {
    await this.checkTokenRevoked(token)

    this.logger.info('Extraindo informações do token')
    const decodeToken = this.tokenUtil.verifyToken(token)
    const decodeRefreshToken = this.tokenUtil.verifyToken(refreshToken)

    if (decodeToken && decodeRefreshToken) {
      if (token === decodeRefreshToken.token) {
        return {
          id: decodeToken.id,
          token,
          refreshToken
        }
      }
    } else if (decodeRefreshToken) {
      this.logger.info('Utilizando refreshToken para gerar novos tokens')

      await this.checkTokenRevoked(<string>decodeRefreshToken.token)

      this.logger.info('Revogando token atual')
      await this.revokeToken(token)

      this.logger.info('Gerando novos tokens')
      const generateToken = this.generateUserToken(decodeRefreshToken.id)
      return {
        id: decodeRefreshToken.id,
        token: generateToken.token,
        refreshToken: generateToken.refreshToken
      }
    }

    throw new ApiErrorException(HttpStatus.UNAUTHORIZED, 'tokenInvalid')
  }

  async checkConnectionKey(
    connectionKey: string
  ): Promise<CheckKeyConnectionResponse> {
    await this.checkConnectionKeyRevoke(connectionKey)
    const keyDecrypt = this.cryptUtil.decryptConnectionKey(connectionKey)

    return {
      id: keyDecrypt.id,
      connectionKey
    }
  }

  async checkConnectionKeyRevoke(connectionKey: string): Promise<void> {
    this.logger.info('Verificando se a connectionKey não está na blacklist...')
    const existConnectionKeyInBlackList = await this.findOneByConnectionKey(
      connectionKey
    )

    if (existConnectionKeyInBlackList) {
      this.logger.error('ConnectionKey está revogado...')
      throw new ApiErrorException(
        HttpStatus.UNAUTHORIZED,
        'connectionKeyRevoked'
      )
    }
    this.logger.info('connectionKey OK')
  }

  async revokeToken(token: string): Promise<void> {
    await this.repository.save({
      typeKey: TokenKeyEnum.TOKEN,
      key: token
    })
  }

  async revokeConnectionKey(connectionKey: string): Promise<void> {
    await this.repository.save({
      typeKey: TokenKeyEnum.CONNECTION_KEY,
      key: connectionKey
    })
  }

  generateUserToken(id: string): GenerateToken {
    return this.generateToken(id)
  }

  generateUserKeyConnection(id: string): string {
    return this.cryptUtil.createConnectionKey(id)
  }

  private async checkTokenRevoked(token: string): Promise<void> {
    this.logger.info('Verificando se o token não está na blacklist...')
    const existTokenInBlackList = await this.findOneByToken(token)

    if (existTokenInBlackList) {
      this.logger.error('Token está revogado...')
      throw new ApiErrorException(HttpStatus.UNAUTHORIZED, 'tokenRevoked')
    }
  }

  async findOneByToken(token: string): Promise<Undefined<TokenBlacklist>> {
    return await this.repository.findOne({
      where: {
        typeKey: 'TOKEN',
        key: token
      }
    })
  }

  async findOneByConnectionKey(
    connectionKey: string
  ): Promise<Undefined<TokenBlacklist>> {
    return await this.repository.findOne({
      where: {
        typeKey: TokenKeyEnum.CONNECTION_KEY,
        key: connectionKey
      }
    })
  }

  private generateToken(id: string): GenerateToken {
    const token = this.tokenUtil.generateToken({
      id
    })

    const refreshToken = this.tokenUtil.generateToken(
      {
        id,
        token
      },
      true
    )

    return {
      token,
      refreshToken
    }
  }
}
