import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'
import { TokenKeyEnum } from '@enums/token-key.enum'

@Entity({ name: 'token_blacklist' })
export class TokenBlacklist {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ name: 'type_key' })
  typeKey!: TokenKeyEnum

  @Column()
  key!: string

  @Column({ name: 'created_at' })
  createdAt!: Date
}
