import { Module } from '@nestjs/common'
import { AuthenticationService } from './authentication.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TokenBlacklist } from '@modules/authentication/token-blacklist.entity'

@Module({
  imports: [TypeOrmModule.forFeature([TokenBlacklist])],
  providers: [AuthenticationService],
  exports: [AuthenticationService]
})
export class AuthenticationModule {}
