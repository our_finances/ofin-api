import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'
import { MovementTypeEnum } from '@enums/movement-type.enum'
import { Profile } from '@modules/profile/profile.entity'
import { Tag } from '@modules/tag/tag.entity'
import { Null } from '@typings/generic.typing'

@Entity({ name: 'movements' })
export class Movement {
  @PrimaryGeneratedColumn('uuid', { name: 'movement_id' })
  movementId!: string

  @Column({
    name: 'movement_type',
    type: 'enum',
    enum: MovementTypeEnum,
    update: false
  })
  movementType!: MovementTypeEnum

  @ManyToOne(() => Profile)
  @JoinColumn({ name: 'profile_id' })
  profile!: Profile

  @Column()
  name!: string

  @Column({ type: 'varchar' })
  description!: Null<string>

  @Column({
    transformer: { from: (v: string) => parseFloat(v), to: (v) => v }
  })
  value!: number

  @Column({ name: 'current_installment' })
  currentInstallment!: number

  @Column({ name: 'installment_amount' })
  installmentAmount!: number

  @Column({ name: 'reference_id' })
  referenceId!: string

  @ManyToOne(() => Tag)
  @JoinColumn({ name: 'tag_id' })
  tag!: Null<Tag>

  @Column({ name: 'due_date', type: 'datetime' })
  dueDate!: Null<Date>

  @Column({ name: 'is_paid' })
  isPaid!: boolean

  @Column({ name: 'registration_date' })
  registrationDate!: Date

  @CreateDateColumn({ name: 'created_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt!: Date
}
