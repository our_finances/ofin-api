import { Module } from '@nestjs/common'
import { MovementController } from '@modules/movement/movement.controller'
import { MovementService } from '@modules/movement/movement.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TagModule } from '@modules/tag/tag.module'
import { ProfileModule } from '@modules/profile/profile.module'
import { MovementRepository } from '@modules/movement/movement.repository'
import { MovementOutcomeService } from '@modules/movement/movement-outcome.service'
import { MovementIncomeService } from '@modules/movement/movement-income.service'

@Module({
  controllers: [MovementController],
  providers: [MovementService, MovementOutcomeService, MovementIncomeService],
  imports: [
    TypeOrmModule.forFeature([MovementRepository]),
    TagModule,
    ProfileModule,
    TagModule
  ],
  exports: [MovementService]
})
export class MovementModule {}
