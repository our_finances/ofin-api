import { HttpStatus, Injectable } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'
import {
  CreateMovementOutcomeRequestDTO,
  EditMovementOutcomeRequestDTO
} from '@typings/movement.typing'
import { Movement } from '@modules/movement/movement.entity'
import { MovementTypeEnum } from '@enums/movement-type.enum'
import { MovementService } from '@modules/movement/movement.service'
import { v4 as uuidV4 } from 'uuid'
import { addMonths, differenceInDays } from 'date-fns'
import { ApiErrorException } from '@exceptions/api-error.exception'

@Injectable()
export class MovementOutcomeService {
  constructor(
    private logger: LoggerAbstract,
    private movementService: MovementService
  ) {
    this.logger.setContext(MovementOutcomeService.name)
  }

  async createMovementOutcome(
    profileId: string,
    data: CreateMovementOutcomeRequestDTO
  ) {
    this.logger.debug(JSON.stringify(data))
    this.checkProfileId(profileId)

    if (data.dueDate && data.registrationDate) {
      this.checkDueDateGreaterOrEqualThanRegistrationDate(
        data.dueDate,
        data.registrationDate
      )
    }

    this.logger.info('Criando o gasto: {}', data.name)

    let amountMovement = data.installments
    const referenceId = uuidV4()
    const registrationDate = data.registrationDate ?? new Date()
    const movementEntities: Movement[] = []

    while (amountMovement--) {
      const movement = new Movement()
      const currentInstallment = data.installments - amountMovement
      movement.movementType = MovementTypeEnum.OUTCOME
      movement.name = data.name
      movement.tag = await this.movementService.getTag(data.tagId)
      movement.description = data.description
      movement.dueDate = data.dueDate
        ? addMonths(data.dueDate, currentInstallment - 1)
        : null
      movement.installmentAmount = data.installments
      movement.currentInstallment = currentInstallment
      movement.referenceId = referenceId
      movement.isPaid = data.isPaid
      movement.value = data.value
      movement.registrationDate = addMonths(
        registrationDate,
        currentInstallment - 1
      )
      movement.profile = await this.movementService.getProfile(profileId)

      movementEntities.push(movement)
    }

    this.logger.info('Salvando {} gasto(s)', movementEntities.length)
    await this.movementService.save(movementEntities)
  }

  async editMovementOutcome(
    profileId: string,
    data: EditMovementOutcomeRequestDTO
  ) {
    this.logger.debug(JSON.stringify(data))
    this.checkProfileId(profileId)

    this.logger.info(
      'Buscando todos os movimentos referentes ao {}',
      data.movementId
    )
    const movements = await this.movementService.getAllReferenceMovement(
      data.movementId,
      profileId
    )

    for (const movement of movements) {
      if (data.name) movement.name = data.name
      if (data.description) movement.description = data.description
      if (data.value) movement.value = data.value
      if (data.isPaid) movement.isPaid = data.isPaid
      if (data.tagId)
        movement.tag = await this.movementService.getTag(data.tagId)
    }

    this.logger.info('Salvando movimentos')
    await this.movementService.save(movements)
  }

  async deleteMovement(profileId: string, movementId: string) {
    this.checkProfileId(profileId)
    this.logger.info('Buscando o movimento {} e suas referencias', movementId)

    const movements = await this.movementService.getAllReferenceMovement(
      movementId,
      profileId
    )

    this.logger.info('Removendo movimentos')
    await this.movementService.delete(movements)
  }

  private checkProfileId(profileId: string) {
    this.movementService.profileIdBelongsToUserAuthenticated(profileId)
  }

  private checkDueDateGreaterOrEqualThanRegistrationDate(
    dueDate: Date,
    registrationDate: Date
  ) {
    if (differenceInDays(dueDate, registrationDate) < 0) {
      throw new ApiErrorException(
        HttpStatus.BAD_REQUEST,
        'dueDateInvalidRegistration'
      )
    }
  }
}
