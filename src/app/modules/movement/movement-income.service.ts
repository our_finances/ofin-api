import { Injectable } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'
import { MovementService } from '@modules/movement/movement.service'
import {
  CreateMovementIncomeRequestDTO,
  EditMovementIncomeRequestDTO
} from '@typings/movement.typing'
import { Movement } from '@modules/movement/movement.entity'
import { MovementTypeEnum } from '@enums/movement-type.enum'

@Injectable()
export class MovementIncomeService {
  constructor(
    private logger: LoggerAbstract,
    private movementService: MovementService
  ) {}

  async createMovementIncome(
    profileId: string,
    data: CreateMovementIncomeRequestDTO
  ) {
    this.checkProfile(profileId)

    this.logger.debug(JSON.stringify(data))
    this.logger.info('Iniciando cadastrado da entrada: {}', data.name)

    const movement = new Movement()

    movement.name = data.name
    movement.movementType = MovementTypeEnum.INCOME
    movement.description = data.description
    movement.value = data.value
    movement.registrationDate = data.registrationDate ?? new Date()
    movement.profile = await this.movementService.getProfile(profileId)
    movement.tag = await this.movementService.getTag(data.tagId)

    this.logger.info('Salvando entrada...')
    await this.movementService.save(movement)
  }

  async editMovementIncome(
    profileId: string,
    data: EditMovementIncomeRequestDTO
  ) {
    this.checkProfile(profileId)
    this.logger.debug(JSON.stringify(data))

    this.logger.info('Buscando pelo movimento: {}', data.movementId)

    const movement = await this.movementService.getMovement(
      data.movementId,
      profileId
    )

    if (data.name) movement.name = data.name
    if (data.description) movement.description = data.description
    if (data.value) movement.value = data.value
    if (data.tagId) movement.tag = await this.movementService.getTag(data.tagId)

    this.logger.info('Salvando entrada...')
    await this.movementService.save(movement)
  }

  async deleteMovementIncome(profileId: string, movementId: string) {
    this.checkProfile(profileId)

    this.logger.info('Buscando pelo movimento: {}', movementId)

    const movement = await this.movementService.getMovement(
      movementId,
      profileId
    )

    this.logger.info('Removendo movimento...')
    await this.movementService.delete(movement)
  }

  private checkProfile(profileId: string) {
    this.movementService.profileIdBelongsToUserAuthenticated(profileId)
  }
}
