import { Movement } from '@modules/movement/movement.entity'
import { EntityRepository, Repository } from 'typeorm'
import { MonthEnum } from '@enums/month.enum'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { HttpStatus } from '@nestjs/common'
import { Profile } from '@modules/profile/profile.entity'
import { User } from '@modules/user/user.entity'
import {
  RawSumMovementOfCurrentMonth,
  SumMovementOfCurrentMonth
} from '@typings/movement.typing'

@EntityRepository(Movement)
export class MovementRepository extends Repository<Movement> {
  findAllByProfileIdAndYearAndMonth(
    profileId: string,
    year: number,
    month: number
  ) {
    return this.createQueryBuilder('mv')
      .leftJoinAndSelect('mv.tag', 'tag')
      .where('month(mv.registration_date) = :monthId', {
        monthId: MonthEnum[month]
      })
      .andWhere('year(mv.registration_date) = :year', { year })
      .andWhere('mv.profile_id = :profileId', { profileId })
      .getMany()
  }

  public async findMovementByIdAndProfileIdOrThrow(
    movementId: string,
    profileId: string
  ) {
    const movement = await this.findOne({
      where: {
        movementId,
        profile: this.buildProfile(profileId)
      }
    })
    if (movement) return movement

    throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'movementNotFound')
  }

  public async findAllMovementWithSameReferenceByMovementIdAndProfileId(
    movementId: string,
    profileId: string
  ) {
    const movement = await this.findMovementByIdAndProfileIdOrThrow(
      movementId,
      profileId
    )

    return await this.find({
      where: {
        referenceId: movement.referenceId,
        profile: this.buildProfile(profileId)
      }
    })
  }

  public async sumIncomeAndOutcomeOfCurrentMonthByUser(user: User) {
    const listProfileId = user.profiles.map((pr) => pr.profileId)
    const result = await this.createQueryBuilder('mv')
      .select('mv.profile_id', 'profileId')
      .addSelect(
        'sum(if(mv.movement_type="INCOME", mv.value, 0))',
        'totalIncome'
      )
      .addSelect(
        'sum(if(mv.movement_type="OUTCOME", mv.value, 0))',
        'totalOutcome'
      )
      .where('month(mv.registration_date) = month(now())')
      .andWhere('year(mv.registration_date) = year(now())')
      .andWhere('mv.profile_id in(:listProfileId)', { listProfileId })
      .addGroupBy('mv.profile_id')
      .getRawMany<RawSumMovementOfCurrentMonth>()

    return result.map((rs) => ({
      ...rs,
      totalOutcome: parseFloat(rs.totalOutcome),
      totalIncome: parseFloat(rs.totalIncome)
    })) as SumMovementOfCurrentMonth[]
  }

  private buildProfile(profileId: string) {
    const profile = new Profile()
    profile.profileId = profileId

    return profile
  }
}
