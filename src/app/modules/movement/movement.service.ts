import { HttpStatus, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { ContextService } from '@modules/context/context.service'
import { Movement } from '@modules/movement/movement.entity'
import { MonthEnum } from '@enums/month.enum'
import removeKeys from '@utils/remove-keys'
import { MovementResponseDTO } from '@typings/movement.typing'
import { MovementRepository } from '@modules/movement/movement.repository'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { LoggerAbstract } from '@logger/logger.abstract'
import { TagService } from '@modules/tag/tag.service'
import { Null } from '@typings/generic.typing'
import { ProfileService } from '@modules/profile/profile.service'

@Injectable()
export class MovementService {
  constructor(
    private logger: LoggerAbstract,
    @InjectRepository(MovementRepository)
    private repository: MovementRepository,
    private tagService: TagService,
    private profileService: ProfileService,
    private ctx: ContextService
  ) {}

  async getMovementsByProfileIdAndYearAndMonth(
    profileId: string,
    year: number,
    month: MonthEnum
  ) {
    this.profileIdBelongsToUserAuthenticated(profileId)
    const listMovement =
      await this.repository.findAllByProfileIdAndYearAndMonth(
        profileId,
        year,
        month
      )

    return this.convertListEntityToListMovementResponseDTO(listMovement)
  }

  public async getSumOfIncomeAndOutcomeOfCurrentMonthByUserAuthenticated() {
    const user = this.getUser()
    return this.repository.sumIncomeAndOutcomeOfCurrentMonthByUser(user)
  }

  public profileIdBelongsToUserAuthenticated(profileId: string) {
    this.logger.info('Verificando se o perfil pertence ao usuário')
    const user = this.getUser()
    const haveProfile = user.profiles.some((pr) => pr.profileId === profileId)
    if (!haveProfile) {
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'profileNotFound')
    }
  }

  public delete(entity: Movement | Movement[]) {
    return this.repository.remove(entity as any)
  }

  public save(entity: Movement | Movement[]) {
    return this.repository.save(entity as any)
  }

  private convertEntityToMovementResponseDTO(
    movement: Movement
  ): MovementResponseDTO {
    return removeKeys(movement, [
      'createdAt',
      'updatedAt',
      'tag.createdAt',
      'tag.updatedAt'
    ])
  }

  private convertListEntityToListMovementResponseDTO(movements: Movement[]) {
    return movements.map(this.convertEntityToMovementResponseDTO)
  }

  private getUser() {
    return this.ctx.getUserOrThrow()
  }

  public getTag(tagId: Null<string>) {
    return tagId ? this.tagService.findOneByTagIdOrThrow(tagId) : null
  }

  public getProfile(profileId: string) {
    return this.profileService.findByUserAuthenticatedAndProfileIdOrThrow(
      profileId
    )
  }

  public getMovement(movementId: string, profileId: string) {
    return this.repository.findMovementByIdAndProfileIdOrThrow(
      movementId,
      profileId
    )
  }

  public getAllReferenceMovement(movementId: string, profileId: string) {
    return this.repository.findAllMovementWithSameReferenceByMovementIdAndProfileId(
      movementId,
      profileId
    )
  }
}
