import { Controller, Delete, Get, Post, Put, Req, Res } from '@nestjs/common'
import { ResponseApi } from '@utils/result.util'
import { Request, Response } from 'express'
import * as zod from 'zod'
import { listMonth, MonthEnum } from '@enums/month.enum'
import { envConfig } from '@config/env.config'
import { isValid } from 'date-fns'
import { MovementOutcomeService } from '@modules/movement/movement-outcome.service'
import { MovementService } from '@modules/movement/movement.service'
import { MovementIncomeService } from '@modules/movement/movement-income.service'

@Controller('profiles/:profileId/movements')
export class MovementController {
  constructor(
    private movementService: MovementService,
    private movementOutcomeService: MovementOutcomeService,
    private movementIncomeService: MovementIncomeService
  ) {}

  @Get(':year/:month')
  async getAllMovements(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaGetMovement = zod.object({
      profileId: zod.string().uuid(),
      month: zod
        .string()
        .refine((month) => listMonth.includes(month.toUpperCase()))
        .transform((v) => v.toUpperCase() as any as MonthEnum),
      year: zod.string().transform((v) => parseInt(v))
    })

    const { profileId, year, month } = schemaGetMovement.parse(req.params)

    const movements =
      await this.movementService.getMovementsByProfileIdAndYearAndMonth(
        profileId,
        year,
        month
      )

    return response.setBody(movements).send()
  }

  @Post('outcome')
  async createMovement(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaCreateMovement = zod.object({
      profileId: zod.string().uuid(),
      name: this.getZodName(),
      description: zod.string().nullable(),
      value: zod.number().positive(),
      installments: zod.number().positive(),
      dueDate: this.getZodDate(),
      registrationDate: this.getZodDate(),
      isPaid: zod.boolean(),
      tagId: zod.string().uuid().nullable()
    })

    const { profileId, ...body } = schemaCreateMovement.parse({
      ...req.body,
      ...req.params
    })

    await this.movementOutcomeService.createMovementOutcome(profileId, body)

    return response.setOk('movementOutcomeCreatedSuccess').send()
  }

  @Put('outcome/:movementId')
  async editMovementOutcome(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaUpdateMovement = zod.object({
      movementId: zod.string().uuid(),
      profileId: zod.string().uuid(),
      name: this.getZodName().optional(),
      description: zod.string().nullable().optional(),
      value: zod.number().positive().optional(),
      tagId: zod.string().nullable().optional(),
      isPaid: zod.boolean().optional()
    })

    const { profileId, ...body } = schemaUpdateMovement.parse({
      ...req.params,
      ...req.body
    })

    await this.movementOutcomeService.editMovementOutcome(profileId, body)

    return response.setOk('movementIncomeUpdatedSuccess').send()
  }

  @Delete('outcome/:movementId')
  async deleteMovementOutcome(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaDeleteMovement = zod.object({
      profileId: zod.string().uuid(),
      movementId: zod.string().uuid()
    })

    const { movementId, profileId } = schemaDeleteMovement.parse({
      ...req.params,
      ...req.body
    })

    await this.movementOutcomeService.deleteMovement(profileId, movementId)

    return response.setOk('movementOutcomeDeletedSuccess').send()
  }

  @Post('income')
  async createMovementIncome(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaCreateMovement = zod.object({
      profileId: zod.string().uuid(),
      name: this.getZodName(),
      description: zod.string().nullable(),
      value: zod.number().positive(),
      registrationDate: this.getZodDate(),
      tagId: zod.string().uuid().nullable()
    })

    const { profileId, ...body } = schemaCreateMovement.parse({
      ...req.params,
      ...req.body
    })

    await this.movementIncomeService.createMovementIncome(profileId, body)

    return response.setOk('movementIncomeCreatedSuccess').send()
  }

  @Put('income/:movementId')
  async editMovementIncome(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaEditMovement = zod.object({
      movementId: zod.string().uuid(),
      profileId: zod.string().uuid(),
      name: this.getZodName().optional(),
      description: zod.string().nullable().optional(),
      value: zod.number().positive().optional(),
      tagId: zod.string().nullable().optional()
    })

    const { profileId, ...body } = schemaEditMovement.parse({
      ...req.params,
      ...req.body
    })

    await this.movementIncomeService.editMovementIncome(profileId, body)

    return response.setOk('movementIncomeUpdatedSuccess').send()
  }

  @Delete('income/:movementId')
  async deleteMovementIncome(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaDeleteMovement = zod.object({
      profileId: zod.string().uuid(),
      movementId: zod.string().uuid()
    })

    const { movementId, profileId } = schemaDeleteMovement.parse({
      ...req.params
    })

    await this.movementIncomeService.deleteMovementIncome(profileId, movementId)

    return response.setOk('movementIncomeDeletedSuccess').send()
  }

  private getZodName() {
    return zod
      .string()
      .min(envConfig.constants.NAME_MIN_LENGTH)
      .max(envConfig.constants.NAME_MAX_LENGTH)
  }

  private getZodDate() {
    return zod
      .number()
      .refine((v) => isValid(new Date(v)))
      .transform((v) => new Date(v))
      .nullable()
  }
}
