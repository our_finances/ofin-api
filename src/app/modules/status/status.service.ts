import { Injectable } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
export class StatusService {
  constructor(private logger: LoggerAbstract) {
    this.logger.setContext(StatusService.name)
  }

  getStatus(): Record<string, string> {
    this.logger.debug({ stats: 'd' })
    return { status: 'ok' }
  }
}
