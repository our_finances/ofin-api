import { Controller, Get } from '@nestjs/common'
import { StatusService } from './status.service'

@Controller('status')
export class StatusController {
  constructor(private statusSrv: StatusService) {}

  @Get()
  getHello(): any {
    return this.statusSrv.getStatus()
  }
}
