import { HttpStatus, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { User } from '@modules/user/user.entity'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { GenderService } from '@modules/gender/gender.service'
import CryptUtil from '@utils/crypt.util'
import { Profile } from '@modules/profile/profile.entity'
import {
  ChangePasswordRequestDTO,
  CreateUserRequestDTO,
  EditUserRequestDTO
} from '@typings/user.typing'
import { DateUtil } from '@utils/date.util'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
export class UserService {
  private cryptUtil: CryptUtil
  private dateUtil: DateUtil

  constructor(
    private logger: LoggerAbstract,
    private genderService: GenderService,
    @InjectRepository(User)
    private repository: Repository<User>
  ) {
    this.logger.setContext(UserService.name)
    this.cryptUtil = new CryptUtil()
    this.dateUtil = new DateUtil()
  }

  findOneByEmail(email: string) {
    return this.repository.findOne({
      where: {
        email
      }
    })
  }

  findOneByUserId(userId: string) {
    return this.repository.findOne({
      where: {
        userId
      },
      relations: ['gender', 'profiles']
    })
  }

  saveUser(user: User) {
    return this.repository.save(user)
  }

  async createUser(data: CreateUserRequestDTO) {
    this.logger.info('Iniciando cadastro de usuário')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando se já existe email cadastrado: ${data.email}`)
    await this.throwIfUserExist(data.email)

    this.dateUtil.checkDate(data.birthDate)

    try {
      this.logger.info('Cadastrando usuário...')
      const defaultProfile = new Profile()
      defaultProfile.name = 'Padrão'
      defaultProfile.isMainProfile = true

      const user = new User()
      user.name = data.name
      user.email = data.email
      user.gender = await this.genderService.findByGenderIdOrThrow(
        data.genderId
      )
      user.birthDate = new Date(data.birthDate)
      user.password = this.cryptUtil.createHash(data.password)
      user.profiles = [defaultProfile]

      const newUser = await this.repository.save(user)

      this.logger.info('Usuário cadastrado com sucesso')
      return await this.findOneByEmail(newUser.email)
    } catch (e) {
      this.logger.error(`Error ao cadastrar usuário: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userCreatedError'
      )
    }
  }

  private async throwIfUserExist(email: string) {
    const existUser = await this.findOneByEmail(email)

    if (existUser) {
      this.logger.error('Usuário já registrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userAlreadyExist')
    }
  }

  async editUser(data: EditUserRequestDTO) {
    this.logger.info('Iniciando edição de usuário')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando pelo usuário: ${data.userId}`)
    const updateUser = await this.findOneByUserId(data.userId)

    if (!updateUser) {
      this.logger.error('Usuário não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userNotFound')
    }

    if (data.genderId) {
      await this.genderService.checkGenderId(data.genderId)
    }
    if (data.birthDate) {
      this.dateUtil.checkDate(data.birthDate)
    }

    try {
      this.logger.info('Atualizando informações...')
      if (data.name) updateUser.name = data.name
      if (data.birthDate) updateUser.birthDate = new Date(data.birthDate)
      if (data.genderId) {
        updateUser.gender = await this.genderService.findByGenderIdOrThrow(
          data.genderId
        )
      }

      await this.repository.save(updateUser)
      this.logger.info('Informação atualizadas')

      return updateUser
    } catch (e) {
      this.logger.error(`Error ao atualizar usuário: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userUpdatedError'
      )
    }
  }

  async changePassword(data: ChangePasswordRequestDTO) {
    this.logger.info('Iniciando mudança de senha')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando usuário: ${data.userId}`)
    const updateUser = await this.findOneByUserId(data.userId)

    if (!updateUser) {
      this.logger.error('Usuário não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userNotFound')
    }

    if (!data.isRecoveryPassword) {
      this.logger.info('Verificando se a senha antiga e a atual')
      if (
        !this.cryptUtil.compareHash(data.currentPassword, updateUser.password)
      ) {
        this.logger.error('Senha antiga não corresponde a atual')
        throw new ApiErrorException(
          HttpStatus.BAD_REQUEST,
          'userOldPasswordNotEqualCurrent'
        )
      }
    } else {
      this.logger.info('Usando mecanismo de recuperação de senha')
    }

    this.logger.info('Verificando se as senhas novas correspondem')
    if (data.newPassword !== data.confirmPassword) {
      this.logger.error('A senhas novas não correspondem')
      throw new ApiErrorException(
        HttpStatus.BAD_REQUEST,
        'userNewPasswordNotEqualConfirm'
      )
    }

    this.logger.info('Alterando a senha...')
    updateUser.password = this.cryptUtil.createHash(data.newPassword)

    await this.repository.save(updateUser)
    this.logger.info('Senha alterada')
  }

  async deleteUser(userId: string): Promise<void> {
    this.logger.info('Iniciando remoção de usuário')

    this.logger.info(`Buscando usuário: ${userId}`)
    const deleteUser = await this.findOneByUserId(userId)

    if (!deleteUser) {
      this.logger.error('Usuário não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userNotFound')
    }

    try {
      this.logger.info('Removendo usuário')
      await this.repository.remove(deleteUser)
      this.logger.info('Usuário removido')
    } catch (e) {
      this.logger.error(`Error ao remover usuário: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userDeletedError'
      )
    }
  }
}
