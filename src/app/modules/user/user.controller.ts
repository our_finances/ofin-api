import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Post,
  Put,
  Req,
  Res
} from '@nestjs/common'
import { Request, Response } from 'express'
import removeKeys from '@utils/remove-keys'
import { ResponseApi } from '@utils/result.util'
import { UserService } from '@modules/user/user.service'
import * as zod from 'zod'
import { envConfig } from '@config/env.config'

@Controller('users')
export class UserController {
  private constants = envConfig.constants

  constructor(private userService: UserService) {}

  @Get('me')
  async getUserAuthenticated(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)
    const user = res.locals.user

    return response
      .setOk()
      .setBody(
        removeKeys(user, [
          'password',
          'isBlocked',
          'genderId',
          'loginAttempts',
          'createdAt',
          'updatedAt',
          'profiles',
          'gender.createdAt',
          'gender.updatedAt'
        ])
      )
      .send()
  }

  @Post()
  async createUser(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaCreateUser = zod.object({
      name: zod.string().min(this.constants.NAME_MIN_LENGTH),
      email: zod.string().email(),
      genderId: zod.number().positive(),
      birthDate: zod.number(),
      password: zod.string().min(this.constants.PASSWORD_MIN_LENGTH)
    })

    const body = schemaCreateUser.parse(req.body)
    await this.userService.createUser(body)

    return response
      .setStatus(HttpStatus.CREATED)
      .setCode('userCreatedSuccess')
      .send()
  }

  @Put()
  async editUser(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaUpdateUser = zod.object({
      userId: zod.string(),
      name: zod.string().min(this.constants.NAME_MIN_LENGTH).optional(),
      genderId: zod.number().optional(),
      birthDate: zod.number().optional()
    })

    const body = schemaUpdateUser.parse({
      userId: res.locals.user.userId,
      ...req.body
    })

    const updateUser = await this.userService.editUser(body)

    return response
      .setOk('userUpdatedSuccess')
      .setBody(
        removeKeys(updateUser, [
          'genderId',
          'password',
          'loginAttempts',
          'createdAt',
          'updatedAt',
          'gender.createdAt',
          'gender.updatedAt'
        ])
      )
      .send()
  }

  @Put('change-password')
  async changePassword(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaChangePassword = zod.object({
      userId: zod.string(),
      currentPassword: zod.string().min(this.constants.PASSWORD_MIN_LENGTH),
      newPassword: zod.string().min(this.constants.PASSWORD_MIN_LENGTH),
      confirmPassword: zod.string().min(this.constants.PASSWORD_MIN_LENGTH)
    })

    const body = schemaChangePassword.parse({
      userId: res.locals.user.userId,
      ...req.body
    })

    await this.userService.changePassword({
      isRecoveryPassword: false,
      ...body
    })

    return response.setOk('userChangePasswordSuccess').send()
  }

  @Delete()
  async deleteUser(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)
    await this.userService.deleteUser(res.locals.user.userId)

    return response.setOk('userDeletedSuccess').send()
  }
}
