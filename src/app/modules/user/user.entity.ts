import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm'
import { Null } from '@typings/generic.typing'
import { Gender } from '@modules/gender/gender.entity'
import { Profile } from '@modules/profile/profile.entity'
import { Tag } from '@modules/tag/tag.entity'

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn('uuid', { name: 'user_id' })
  userId!: string

  @Column()
  name!: string

  @Column()
  email!: string

  @ManyToOne(() => Gender)
  @JoinColumn({ name: 'gender_id' })
  gender!: Gender

  @Column({ name: 'birth_date' })
  birthDate!: Date

  @Column()
  password!: string

  @Column({ name: 'login_attempts' })
  loginAttempts!: number

  @Column({ name: 'is_blocked', type: 'varchar' })
  isBlocked?: Null<Date>

  @OneToMany(() => Profile, (profile) => profile.user, {
    cascade: true
  })
  profiles!: Profile[]

  @OneToMany(() => Tag, (tag) => tag.user, {
    cascade: true
  })
  tags!: Tag[]
}
