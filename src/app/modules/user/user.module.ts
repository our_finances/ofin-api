import { Module } from '@nestjs/common'
import { UserService } from '@modules/user/user.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from '@modules/user/user.entity'
import { UserController } from '@modules/user/user.controller'
import { GenderModule } from '@modules/gender/gender.module'

@Module({
  controllers: [UserController],
  imports: [TypeOrmModule.forFeature([User]), GenderModule],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
