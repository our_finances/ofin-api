import { Module } from '@nestjs/common'
import { ProfileViewService } from '@modules/profileView/profile-view.service'
import { ProfileModule } from '@modules/profile/profile.module'
import { MovementModule } from '@modules/movement/movement.module'
import { ProfileViewController } from '@modules/profileView/profile-view.controller'

@Module({
  controllers: [ProfileViewController],
  providers: [ProfileViewService],
  imports: [ProfileModule, MovementModule]
})
export class ProfileViewModule {}
