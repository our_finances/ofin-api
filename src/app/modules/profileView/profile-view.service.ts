import { Injectable } from '@nestjs/common'
import { MovementService } from '@modules/movement/movement.service'
import { ProfileService } from '@modules/profile/profile.service'
import { LoggerAbstract } from '@logger/logger.abstract'
import { ProfileWithBalance } from '@typings/user-profile.typing'

@Injectable()
export class ProfileViewService {
  constructor(
    private logger: LoggerAbstract,
    private movementService: MovementService,
    private profileService: ProfileService
  ) {}

  async getAllProfileByUserAuthenticated() {
    const profilesWithBalance =
      await this.profileService.getAllProfileByUserAuthenticated()
    const movements =
      await this.movementService.getSumOfIncomeAndOutcomeOfCurrentMonthByUserAuthenticated()

    const resultSum: ProfileWithBalance[] = []

    profilesWithBalance.forEach((pr) => {
      const movement = movements.find((mv) => mv.profileId === pr.profileId)
      if (movement) {
        pr.totalIncome += movement.totalIncome
        pr.totalOutcome += movement.totalOutcome
      }
      resultSum.push(pr)
    })
    return resultSum
  }
}
