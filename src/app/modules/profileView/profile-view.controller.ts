import { Controller, Get, Req, Res } from '@nestjs/common'
import { Request, Response } from 'express'
import { ResponseApi } from '@utils/result.util'
import { ProfileViewService } from '@modules/profileView/profile-view.service'

@Controller('/profiles')
export class ProfileViewController {
  constructor(private profileViewService: ProfileViewService) {}

  @Get()
  async getAllProfiles(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const profiles =
      await this.profileViewService.getAllProfileByUserAuthenticated()
    return response.setBody(profiles).send()
  }
}
