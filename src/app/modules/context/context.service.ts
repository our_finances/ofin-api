import { HttpStatus, Injectable } from '@nestjs/common'
import { getContext } from '@utils/context.util'
import { ApiErrorException } from '@exceptions/api-error.exception'

@Injectable()
export class ContextService {
  getUser() {
    return getContext('user')
  }

  getUserOrThrow() {
    const user = getContext('user')
    if (user) return user
    throw new ApiErrorException(
      HttpStatus.INTERNAL_SERVER_ERROR,
      'userNotFound'
    )
  }
}
