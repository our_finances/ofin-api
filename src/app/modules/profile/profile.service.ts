import { HttpStatus, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Profile } from '@modules/profile/profile.entity'

import {
  CreateUserProfileRequestDTO,
  DeleteUserProfileRequestDTO,
  ProfileWithBalance,
  ProfileResponseDTO,
  UpdateUserProfileRequestDTO
} from '@typings/user-profile.typing'
import removeKeys from '@utils/remove-keys'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { ContextService } from '@modules/context/context.service'
import { LoggerAbstract } from '@logger/logger.abstract'
import { ProfileRepository } from '@modules/profile/profile.repository'

@Injectable()
export class ProfileService {
  constructor(
    private logger: LoggerAbstract,
    @InjectRepository(ProfileRepository)
    private repository: ProfileRepository,
    private ctx: ContextService
  ) {}

  async getAllProfileByUserAuthenticated() {
    const user = this.getUser()
    return await this.repository.findByUser(user)
  }

  async findOneByUserAuthenticatedAndProfileId(profileId: string) {
    const user = this.getUser()
    return await this.repository.findByUserAndProfileId(user, profileId)
  }

  async findByUserAuthenticatedAndProfileIdOrThrow(profileId: string) {
    const profile = await this.findOneByUserAuthenticatedAndProfileId(profileId)
    if (profile) return profile

    throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'profileNotFound')
  }

  async createProfile(data: CreateUserProfileRequestDTO) {
    this.logger.info('Iniciando cadastrado de perfil')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    const user = this.getUser()

    const createProfile = new Profile()
    createProfile.name = data.name
    createProfile.isMainProfile = false
    createProfile.user = user

    try {
      const profile = await this.repository.save(createProfile)
      this.logger.info(`Perfil ${data.name} criado com sucesso`)
      return this.convertEntityToUserProfileResponseDTO(profile)
    } catch (e) {
      this.logger.error(`Error ao criar perfil: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'profileCreatedError'
      )
    }
  }

  async updateProfile(data: UpdateUserProfileRequestDTO) {
    this.logger.info('Iniciando atualização de perfil')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando perfil: ${data.profileId}`)
    const updateProfile = await this.findOneByUserAuthenticatedAndProfileId(
      data.profileId
    )

    if (!updateProfile) {
      this.logger.error('Perfil não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'profileNotFound')
    }

    try {
      this.logger.info('Atualizando informação')
      if (data.name) updateProfile.name = data.name

      await this.repository.save(updateProfile)
      this.logger.info('Perfil atualizado com sucesso')
      return this.convertEntityToUserProfileResponseDTO(updateProfile)
    } catch (e) {
      this.logger.error(`Error ao atualizar perfil: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'profileUpdatedError'
      )
    }
  }

  async deleteProfile(data: DeleteUserProfileRequestDTO) {
    this.logger.info('Iniciando remoção do perfil')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info(`Buscando o perfil: ${data.profileId}`)

    const deleteProfile = await this.findOneByUserAuthenticatedAndProfileId(
      data.profileId
    )

    if (!deleteProfile) {
      this.logger.error('Perfil não encontrado')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'profileNotFound')
    }

    if (deleteProfile.isMainProfile) {
      this.logger.error('Não pode apagar o perfil padrão')
      throw new ApiErrorException(
        HttpStatus.BAD_REQUEST,
        'profileNotDeleteDefault'
      )
    }

    try {
      this.logger.info('Removendo perfil...')
      await this.repository.remove(deleteProfile)
      this.logger.info('Perfil removido')
    } catch (e) {
      this.logger.error('Error ao remover')
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'profileDeletedError'
      )
    }
  }

  private convertEntityToUserProfileResponseDTO(
    userProfile: Profile
  ): ProfileResponseDTO {
    return removeKeys(userProfile, ['user', 'createdAt', 'updatedAt'])
  }

  private convertListEntityToListUserProfileResponseDTO(
    userProfileList: Profile[]
  ) {
    return userProfileList.map(this.convertEntityToUserProfileResponseDTO)
  }

  private getUser() {
    return this.ctx.getUserOrThrow()
  }
}
