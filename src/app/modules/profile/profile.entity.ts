import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'
import { User } from '@modules/user/user.entity'
import { ProfileBalance } from '@modules/profileBalance/profile-balance.entity'

@Entity({ name: 'user_profiles' })
export class Profile {
  @PrimaryGeneratedColumn('uuid', { name: 'profile_id' })
  profileId!: string

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user!: User

  @OneToMany(() => ProfileBalance, (balance) => balance.profile)
  balances!: ProfileBalance[]

  @Column()
  name!: string

  @Column({ name: 'main_profile' })
  isMainProfile!: boolean

  @CreateDateColumn({ name: 'created_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt!: Date
}
