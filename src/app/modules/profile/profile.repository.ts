import { EntityRepository, Repository } from 'typeorm'
import { Profile } from '@modules/profile/profile.entity'
import { User } from '@modules/user/user.entity'
import {
  ProfileWithBalance,
  RawProfileWithBalance
} from '@typings/user-profile.typing'

@EntityRepository(Profile)
export class ProfileRepository extends Repository<Profile> {
  async findByUser(user: User) {
    const result = await this.createQueryBuilder('pr')
      .select('pr.profile_id', 'profileId')
      .addSelect('pr.main_profile', 'mainProfile')
      .addSelect('pr.name', 'name')
      .addSelect('sum(ifnull(pbm.total_income, 0))', 'totalIncome')
      .addSelect('sum(ifnull(pbm.total_outcome, 0))', 'totalOutcome')
      .leftJoin('pr.balances', 'pbm')
      .where('pr.user_id = :userId', { userId: user.userId })
      .addGroupBy('pr.profile_id')
      .getRawMany<RawProfileWithBalance>()

    return result.map((rs) => ({
      ...rs,
      totalIncome: parseFloat(rs.totalIncome),
      totalOutcome: parseFloat(rs.totalOutcome)
    })) as ProfileWithBalance[]
  }

  findByUserAndProfileId(user: User, profileId: string) {
    return this.findOne({
      where: {
        user,
        profileId
      }
    })
  }
}
