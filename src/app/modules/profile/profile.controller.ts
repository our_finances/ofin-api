import { Controller, Delete, Post, Put, Req, Res } from '@nestjs/common'
import { Request, Response } from 'express'
import { ResponseApi } from '@utils/result.util'
import { ProfileService } from '@modules/profile/profile.service'
import * as zod from 'zod'
import { envConfig } from '@config/env.config'

@Controller('profiles')
export class ProfileController {
  constructor(private userProfileService: ProfileService) {}

  @Post()
  async createUserProfile(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaCreateProfile = zod.object({
      name: zod.string().min(envConfig.constants.NAME_MIN_LENGTH)
    })

    const body = schemaCreateProfile.parse(req.body)

    const profile = await this.userProfileService.createProfile(body)

    return response.setOk('profileCreatedSuccess').setBody(profile).send()
  }

  @Put(':profileId')
  async updateProfile(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaUpdateProfile = zod.object({
      profileId: zod.string().uuid(),
      name: zod.string().optional()
    })

    const body = schemaUpdateProfile.parse({
      profileId: req.params.profileId,
      ...req.body
    })

    const updateProfile = await this.userProfileService.updateProfile(body)

    return response.setOk('profileUpdatedSuccess').setBody(updateProfile).send()
  }

  @Delete(':profileId')
  async deleteProfile(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaDeleteProfile = zod.object({
      profileId: zod.string().uuid()
    })

    const body = schemaDeleteProfile.parse({
      profileId: req.params.profileId
    })

    await this.userProfileService.deleteProfile(body)

    return response.setOk('profileDeletedSuccess').send()
  }
}
