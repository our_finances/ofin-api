import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProfileController } from '@modules/profile/profile.controller'
import { ProfileService } from '@modules/profile/profile.service'
import { ProfileRepository } from '@modules/profile/profile.repository'

@Module({
  controllers: [ProfileController],
  providers: [ProfileService],
  imports: [TypeOrmModule.forFeature([ProfileRepository])],
  exports: [ProfileService]
})
export class ProfileModule {}
