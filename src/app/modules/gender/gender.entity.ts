import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'

@Entity({ name: 'genders' })
export class Gender {
  @PrimaryGeneratedColumn({ name: 'gender_id' })
  genderId!: number

  @Column()
  name!: string

  @CreateDateColumn({ name: 'updated_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'created_at' })
  updatedAt!: Date
}
