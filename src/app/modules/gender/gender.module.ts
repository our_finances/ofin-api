import { Module } from '@nestjs/common'
import { GenderService } from '@modules/gender/gender.service'
import { GenderController } from '@modules/gender/gender.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Gender } from '@modules/gender/gender.entity'

@Module({
  controllers: [GenderController],
  providers: [GenderService],
  imports: [TypeOrmModule.forFeature([Gender])],
  exports: [GenderService]
})
export class GenderModule {}
