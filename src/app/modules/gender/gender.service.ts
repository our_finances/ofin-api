import { InjectRepository } from '@nestjs/typeorm'
import { Gender } from '@modules/gender/gender.entity'
import { Repository } from 'typeorm'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { HttpStatus } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'

export class GenderService {
  constructor(
    private logger: LoggerAbstract,
    @InjectRepository(Gender) private repository: Repository<Gender>
  ) {
    this.logger.setContext(GenderService.name)
  }

  getAllGender() {
    return this.repository.find()
  }

  async checkGenderId(genderId: number): Promise<void> {
    this.logger.info(`Verificando o gênero [ ${genderId} ] existe`)
    const listGenderId = await this.findOneByGenderId(genderId)
    if (!listGenderId) {
      this.logger.error('Gênero não existe')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'genderNotFound')
    }
    this.logger.info('Gênero existe')
  }

  findOneByGenderId(genderId: number) {
    return this.repository.findOne({
      where: {
        genderId
      }
    })
  }

  async findByGenderIdOrThrow(genderId: number) {
    const gender = await this.findOneByGenderId(genderId)
    if (gender) return gender
    throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'genderNotFound')
  }
}
