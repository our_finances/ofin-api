import { Controller, Get, Req, Res } from '@nestjs/common'
import { Request, Response } from 'express'
import { ResponseApi } from '@utils/result.util'
import { GenderService } from '@modules/gender/gender.service'

@Controller('genders')
export class GenderController {
  constructor(private genderService: GenderService) {}

  @Get()
  async getAllGender(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const genderList = await this.genderService.getAllGender()

    return response.setBody(genderList).send()
  }
}
