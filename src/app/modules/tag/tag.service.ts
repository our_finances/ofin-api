import { HttpStatus, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Tag } from '@modules/tag/tag.entity'
import { Repository } from 'typeorm'
import { ContextService } from '@modules/context/context.service'
import removeKeys from '@utils/remove-keys'
import {
  CreateTagRequestDTO,
  DeleteTagRequestDTO,
  EditTagRequestDTO,
  TagResponseDTO
} from '@typings/tag.typing'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
export class TagService {
  constructor(
    private logger: LoggerAbstract,
    private ctx: ContextService,
    @InjectRepository(Tag) private repository: Repository<Tag>
  ) {}

  async getAllTagByUserAuthenticated() {
    const user = this.getUser()
    const tags = await this.repository.find({
      where: {
        user
      }
    })
    return this.convertListEntityToListTagResponseDTO(tags)
  }

  async findOneByTagIdOrThrow(tagId: string) {
    const user = this.getUser()
    const tag = await this.repository.findOne({
      where: {
        tagId,
        user
      }
    })
    if (tag) return tag

    this.logger.error('Nenhuma tag encontrada')
    throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userTagNotFound')
  }

  async createUserTag(data: CreateTagRequestDTO) {
    this.logger.info('Iniciando criação de uma nova tag')
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    await this.throwIfExistTagName(data.name)

    try {
      this.logger.info('Criando a nova tag...')
      const createTag = new Tag()
      createTag.name = data.name
      createTag.color = data.color
      createTag.user = this.getUser()

      const newTag = await this.repository.save(createTag)

      this.logger.info('Nova tag criada')
      return this.convertEntityToTagResponseDTO(newTag)
    } catch (e) {
      this.logger.error(`Erro ao criar nova tag: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userTagCreatedError'
      )
    }
  }

  async updateUserTag(data: EditTagRequestDTO) {
    this.logger.info(`Iniciando atualização da tag: ${data.tagId}`)
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info('Buscando dados da tag')
    const updateUserTag = await this.findOneByTagIdOrThrow(data.tagId)

    if (data.name) {
      await this.throwIfExistTagName(data.name, data.tagId)
    }

    try {
      this.logger.info('Atualizando tag...')
      if (data.name) updateUserTag.name = data.name
      if (data.color) updateUserTag.color = data.color

      await this.repository.save(updateUserTag)
      this.logger.info('Tag atualizada com sucesso')

      return this.convertEntityToTagResponseDTO(updateUserTag)
    } catch (e) {
      this.logger.error(`Erro ao atualizar tag: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userTagUpdatedError'
      )
    }
  }

  async deleteUserTag(data: DeleteTagRequestDTO) {
    this.logger.info(`Iniciando remoção da tag: ${data.tagId}`)
    this.logger.debug(`Data: ${JSON.stringify(data)}`)

    this.logger.info('Buscando pela tag...')
    const deleteUserTag = await this.findOneByTagIdOrThrow(data.tagId)

    try {
      this.logger.info('Removendo tag...')

      await this.repository.remove(deleteUserTag)
      this.logger.info('Tag removida com sucesso')
    } catch (e) {
      this.logger.error(`Erro ao remover tag: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'userTagDeletedError'
      )
    }
  }

  private async throwIfExistTagName(name: string, tagId?: string) {
    this.logger.info(`Verificando se já existe um com o nome: ${name}`)
    const user = this.getUser()
    const tagList = await this.repository.find({
      where: {
        user
      }
    })

    const tagIndex = tagList.findIndex(
      (it) => it.name.toLowerCase() === name.toLowerCase()
    )
    if (tagIndex !== -1 && tagList[tagIndex].tagId !== tagId) {
      this.logger.error('Já existe uma tag com o mesmo nome')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'userTagExistName')
    }
  }

  private convertListEntityToListTagResponseDTO(tags: Tag[]) {
    return tags.map(this.convertEntityToTagResponseDTO)
  }

  private convertEntityToTagResponseDTO(tag: Tag): TagResponseDTO {
    return removeKeys(tag, ['user', 'createdAt', 'updatedAt'])
  }

  private getUser() {
    return this.ctx.getUserOrThrow()
  }
}
