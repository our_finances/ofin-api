import { Controller, Delete, Get, Post, Put, Req, Res } from '@nestjs/common'
import { TagService } from '@modules/tag/tag.service'
import { Request, Response } from 'express'
import { ResponseApi } from '@utils/result.util'
import * as zod from 'zod'
import { envConfig } from '@config/env.config'

@Controller('tags')
export class TagController {
  constructor(private tagService: TagService) {}

  @Get()
  async getAllTags(@Res() res: Response, @Req() req: Request) {
    const response = new ResponseApi(res)

    const tags = await this.tagService.getAllTagByUserAuthenticated()

    return response.setBody(tags).send()
  }

  @Post()
  async createTag(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaCreateTag = zod.object({
      name: zod.string().min(envConfig.constants.NAME_MIN_LENGTH),
      color: zod.string()
    })

    const body = schemaCreateTag.parse(req.body)

    const newTag = await this.tagService.createUserTag(body)

    return response.setOk('userTagCreatedSuccess').setBody(newTag).send()
  }

  @Put(':tagId')
  async updateTag(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaUpdateTag = zod.object({
      tagId: zod.string().uuid(),
      name: zod.string().optional(),
      color: zod.string().optional()
    })

    const body = schemaUpdateTag.parse({
      ...req.body,
      ...req.params
    })

    const updateTag = await this.tagService.updateUserTag(body)

    return response.setOk('userTagUpdatedSuccess').setBody(updateTag).send()
  }

  @Delete(':tagId')
  async deleteTag(@Req() req: Request, @Res() res: Response) {
    const response = new ResponseApi(res)

    const schemaDeleteTag = zod.object({
      tagId: zod.string().uuid()
    })

    const body = schemaDeleteTag.parse({
      ...req.params
    })

    await this.tagService.deleteUserTag(body)

    return response.setOk('userTagDeletedSuccess').send()
  }
}
