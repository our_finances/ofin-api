import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Tag } from '@modules/tag/tag.entity'
import { TagService } from '@modules/tag/tag.service'
import { TagController } from '@modules/tag/tag.controller'

@Module({
  controllers: [TagController],
  imports: [TypeOrmModule.forFeature([Tag])],
  providers: [TagService],
  exports: [TagService]
})
export class TagModule {}
