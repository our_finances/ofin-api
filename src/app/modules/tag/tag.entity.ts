import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'
import { User } from '@modules/user/user.entity'

@Entity({ name: 'user_tags' })
export class Tag {
  @PrimaryGeneratedColumn('uuid', { name: 'tag_id' })
  tagId!: string

  @Column()
  name!: string

  @Column()
  color!: string

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user!: User

  @CreateDateColumn({ name: 'created_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt!: Date
}
