import { Injectable } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'
import { InjectRepository } from '@nestjs/typeorm'
import { ProfileBalanceRepository } from '@modules/profileBalance/profile-balance.repository'

@Injectable()
export class ProfileBalanceService {
  constructor(
    private logger: LoggerAbstract,
    @InjectRepository(ProfileBalanceRepository)
    private repository: ProfileBalanceRepository
  ) {}
}
