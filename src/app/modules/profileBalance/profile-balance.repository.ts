import { EntityRepository, Repository } from 'typeorm'
import { ProfileBalance } from '@modules/profileBalance/profile-balance.entity'

@EntityRepository(ProfileBalance)
export class ProfileBalanceRepository extends Repository<ProfileBalance> {}
