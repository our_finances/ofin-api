import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'
import { Profile } from '@modules/profile/profile.entity'

@Entity({ name: 'profile_balance_monthly' })
export class ProfileBalance {
  @PrimaryGeneratedColumn('uuid', { name: 'balance_id' })
  balanceId!: string

  @ManyToOne(() => Profile)
  @JoinColumn({ name: 'profile_id' })
  profile!: Profile

  @Column({
    type: 'decimal',
    name: 'total_income',
    transformer: { from: (v: string) => parseFloat(v), to: (v) => v }
  })
  totalIncome!: number

  @Column({
    type: 'decimal',
    name: 'total_outcome',
    transformer: { from: (v: string) => parseFloat(v), to: (v) => v }
  })
  totalOutcome!: number

  @Column({ name: 'month' })
  month!: number

  @Column({ name: 'year' })
  year!: number

  @CreateDateColumn({ name: 'created_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt!: Date
}
