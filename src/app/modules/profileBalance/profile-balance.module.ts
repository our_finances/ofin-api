import { Module } from '@nestjs/common'
import { ProfileBalanceService } from '@modules/profileBalance/profile-balance.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProfileBalanceRepository } from '@modules/profileBalance/profile-balance.repository'

@Module({
  exports: [ProfileBalanceService],
  providers: [ProfileBalanceService],
  imports: [TypeOrmModule.forFeature([ProfileBalanceRepository])]
})
export class ProfileBalanceModule {}
