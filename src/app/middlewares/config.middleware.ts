import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response, NextFunction } from 'express'
import { setContext } from '@utils/context.util'
import { differenceInMilliseconds } from 'date-fns'
import * as randomBytes from 'random-bytes'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
export class ConfigMiddleware implements NestMiddleware {
  constructor(private logger: LoggerAbstract) {
    this.logger.setContext(ConfigMiddleware.name)
  }

  use(req: Request, res: Response, next: NextFunction) {
    res.locals.timeIni = new Date().getTime()
    setContext('req', {
      originalUrl: req.originalUrl,
      method: req.method,
      host: `${req.protocol}://${req.get('host')}`,
      requestId: randomBytes.sync(12).toString('hex') || '-'
    })

    res.on('finish', () => {
      this.logger.info(
        `Tempo de execução: ${differenceInMilliseconds(
          new Date(),
          res.locals.timeIni
        )} ms`
      )
    })

    next()
  }
}
