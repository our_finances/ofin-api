import { NextFunction, Request, Response } from 'express'
import { setContext } from '@utils/context.util'
import { CheckTokenResponse } from '@typings/token.typing'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common'
import { AuthenticationService } from '@modules/authentication/authentication.service'
import { UserService } from '@modules/user/user.service'
import { LoggerAbstract } from '@logger/logger.abstract'

@Injectable()
class AuthMiddleware implements NestMiddleware {
  private static REFRESH_TOKEN = 'refresh-token'
  private static CONNECTION_KEY = 'connection-key'

  constructor(
    private logger: LoggerAbstract,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.logger.setContext(AuthMiddleware.name)
  }

  async use(req: Request, res: Response, next: NextFunction) {
    await this.user(req, res, next)
  }

  private async tokenInfo(req: Request): Promise<CheckTokenResponse> {
    this.logger.debug(`Headers: ${JSON.stringify(req.headers)}`)
    if (req.headers[AuthMiddleware.CONNECTION_KEY]) {
      this.logger.info('Autorização via connectionKey')
      return await this.authenticationService.checkConnectionKey(
        <string>req.headers[AuthMiddleware.CONNECTION_KEY]
      )
    } else if (
      req.headers.authorization &&
      req.headers[AuthMiddleware.REFRESH_TOKEN]
    ) {
      this.logger.info('Autorização via token')
      const refreshToken = <string>req.headers[AuthMiddleware.REFRESH_TOKEN]
      const [bearer, token] = req.headers.authorization.split(' ')
      const checkToken = await this.authenticationService.checkToken(
        token,
        refreshToken
      )

      if (bearer === 'Bearer' && checkToken) {
        return checkToken
      }
    }

    this.logger.error('Requisição não possui token ou chave de acesso')
    throw new ApiErrorException(
      HttpStatus.UNAUTHORIZED,
      'withoutTokenOrConnectionKey'
    )
  }

  async user(req: Request, res: Response, next: NextFunction) {
    this.logger.info('Iniciando autenticação de usuário')
    const decode = await this.tokenInfo(req)

    if (decode.connectionKey) {
      this.logger.info('Autorizado via connectionKey')
      res.locals.token = undefined
      res.locals.refreshToken = undefined
      res.locals.connectionKey = decode.connectionKey
    } else {
      this.logger.info('Autorizado via token')
      res.locals.connectionKey = undefined
      res.locals.token = decode.token
      res.locals.refreshToken = decode.refreshToken
    }

    this.logger.info('Buscando usuário')
    const userId = decode.id
    const userData = await this.userService.findOneByUserId(userId)

    if (!userData) {
      this.logger.error(`Usuário [ ${userId} ] não existe`)
      throw new ApiErrorException(HttpStatus.UNAUTHORIZED, 'userNotFound')
    }

    this.logger.info('Salvando usuário no contexto')
    setContext('user', userData)
    res.locals.user = userData

    next()
  }
}

export default AuthMiddleware
