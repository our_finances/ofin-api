import * as httpContext from 'express-http-context'
import { User } from '@modules/user/user.entity'
import { Undefined } from '@typings/generic.typing'
import { Request, Response } from 'express'

export interface contextKeys {
  req: {
    method: string
    host: string
    originalUrl: string
    requestId: string
  }
  user: User
}

export const setContext = <K extends keyof contextKeys>(
  key: K,
  data: contextKeys[K]
): void => {
  httpContext.set(key, data)
}

export const getContext = <K extends keyof contextKeys>(
  key: K
): Undefined<contextKeys[K]> => {
  return httpContext.get(key)
}
