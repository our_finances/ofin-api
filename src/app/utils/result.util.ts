import { Response as Res } from 'express'
import { messages } from './translate'
import { HttpStatus } from '@nestjs/common'
import { Undefined } from '@typings/generic.typing'

export class ResponseApi<T> {
  private status = HttpStatus.OK
  private response: Res
  private code: messages = 'requestDone'
  private data: Undefined<T> = undefined
  private token: Undefined<string> = undefined
  private refreshToken: Undefined<string> = undefined
  private connectionKey: Undefined<string> = undefined

  constructor(res: Res) {
    this.response = res
  }

  setOk(code: messages = 'requestDone') {
    this.status = HttpStatus.OK
    this.code = code
    return this
  }

  setStatus(status: HttpStatus) {
    this.status = status
    return this
  }

  setCode(code: messages) {
    this.code = code
    return this
  }

  setBody(data: T) {
    this.data = data
    return this
  }

  setToken(token: Undefined<string>) {
    this.token = token
    return this
  }

  setRefreshToken(refreshToken: Undefined<string>) {
    this.refreshToken = refreshToken
    return this
  }

  setConnectionKey(connectionKey: Undefined<string>) {
    this.connectionKey = connectionKey
    return this
  }

  send() {
    this.response.status(this.status).send({
      status: `${this.status} - ${HttpStatus[this.status]}`,
      code: this.code,
      msg: messages[this.code],
      body: this.data,
      token: this.token || this.response.locals.token,
      refreshToken: this.refreshToken || this.response.locals.refreshToken,
      connectionKey: this.connectionKey
    })
  }
}
