import { WinstonLoggerService } from '@logger/winston-logger.service'
import { differenceInBusinessDays, format, isValid } from 'date-fns'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { HttpStatus } from '@nestjs/common'
import { LoggerAbstract } from '@logger/logger.abstract'

export class DateUtil {
  private logger: LoggerAbstract

  constructor() {
    this.logger = new WinstonLoggerService()
    this.logger.setContext(DateUtil.name)
  }

  checkDueDate(date: string): void {
    this.logger.info(
      'Verificando se a data de vencimento é posterior a data atual'
    )
    this.checkDate(date)
    if (
      differenceInBusinessDays(
        new Date(date),
        new Date(format(new Date(), 'yyyy-MM-dd'))
      ) === 0
    ) {
      this.logger.error(
        'A data de vencimento deve ser maior que a data de hoje'
      )
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'dueDateInvalid')
    }
  }

  checkDate(date: any): void {
    this.logger.info(`Verificando a data [ ${date} ] está correta`)
    if (!isValid(date)) {
      this.logger.error('A data é inválida')
      throw new ApiErrorException(HttpStatus.BAD_REQUEST, 'dateInvalid')
    }
  }

  formatDate(date: string): Date {
    return new Date(date.replace('-', '/'))
  }
}
