import jwt from 'jsonwebtoken'
import CryptUtil from './crypt.util'
import { TokenArray, JwtObj } from '@typings/token.typing'

class TokenUtil {
  secretKeyToken: string
  SESSION_TIME: number
  private cryptUtil = new CryptUtil()

  constructor() {
    this.secretKeyToken = String(process.env.SECRET_KEY_TOKEN)
    this.SESSION_TIME = Number(process.env.SESSION_TIME)
  }

  generateToken(value: TokenArray, refresh = false): string {
    const obj: JwtObj = {
      key: this.cryptUtil.encrypt(JSON.stringify(value)).toString()
    }

    return jwt.sign(obj, this.secretKeyToken, {
      algorithm: 'HS256',
      expiresIn: 60 * this.SESSION_TIME * (refresh ? 2 : 1)
    })
  }

  verifyToken(token: string): TokenArray {
    const ff = <JwtObj>jwt.verify(token, this.secretKeyToken)
    return JSON.parse(this.cryptUtil.decrypt(ff.key))
  }
}

export default TokenUtil
