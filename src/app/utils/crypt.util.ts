import bcrypt from 'bcryptjs'
import CryptoJS from 'crypto-js'
import { WinstonLoggerService } from '@logger/winston-logger.service'
import { ApiErrorException } from '@exceptions/api-error.exception'
import { HttpStatus } from '@nestjs/common'

class CryptUtil {
  private logger = new WinstonLoggerService()
  private salt = bcrypt.genSaltSync(14)
  private secretKey: string

  constructor() {
    this.logger.setContext(CryptUtil.name)
    this.secretKey = String(process.env.SECRET_KEY_CRYPT)
  }

  createHash(value: string): string {
    return bcrypt.hashSync(value, this.salt)
  }

  compareHash(value: string, hash: string): boolean {
    return bcrypt.compareSync(value, hash)
  }

  encrypt(value: string): string {
    return CryptoJS.AES.encrypt(value, this.secretKey).toString()
  }

  decrypt(value: string): string {
    return CryptoJS.AES.decrypt(value.toString(), this.secretKey).toString(
      CryptoJS.enc.Utf8
    )
  }

  compareCrypt(value: string, valueCrypt: string): boolean {
    return (
      CryptoJS.AES.decrypt(valueCrypt.toString(), this.secretKey).toString(
        CryptoJS.enc.Utf8
      ) === value
    )
  }

  createConnectionKey(id: string): string {
    return this.encrypt(
      JSON.stringify({
        id
      })
    )
  }

  decryptConnectionKey(keyEncrypted: string): { id: string } {
    try {
      const decryptKey = this.decrypt(keyEncrypted)
      this.logger.debug(`decryptKey: ${decryptKey}`)
      return JSON.parse(decryptKey)
    } catch (e) {
      this.logger.error(`Error ao decriptar connectionKey: ${e}`)
      throw new ApiErrorException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        'connectionKeyDecryptError'
      )
    }
  }
}

export default CryptUtil
