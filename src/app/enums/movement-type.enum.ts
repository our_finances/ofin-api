export enum MovementTypeEnum {
  INCOME = 'INCOME',
  OUTCOME = 'OUTCOME'
}
