export enum MonthEnum {
  JANUARY = 1,
  FEBRUARY,
  MARCH,
  APRIL,
  MAY,
  JUNE,
  JULY,
  AUGUST,
  SEPTEMBER,
  OCTOBER,
  NOVEMBER,
  DECEMBER
}

const listMonthData = Object.keys(MonthEnum)

export const listMonth = listMonthData.slice(
  listMonthData.length / 2,
  listMonthData.length
)

export const listMonthObject = listMonth.map((month, index) => ({
  id: index + 1,
  name: month
}))
