const env = process.env

export const envConfig = {
  port: env.PORT ?? 3001,
  dbConfig: {
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT ?? '3306'),
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  },
  constants: {
    REGEX_UUID:
      /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i,
    REGEX_DATE_FORMAT: /[0-9]{4}-[0-9]{2}-[0-9]{2}/,
    PASSWORD_MIN_LENGTH: 6,
    NAME_MIN_LENGTH: 3,
    NAME_MAX_LENGTH: 25
  }
}
