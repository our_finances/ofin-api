import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import path from 'path'

export const typeormConfig = (env: any): TypeOrmModuleOptions => {
  console.log({
    type: 'mysql',
    host: env.MYSQL_HOST,
    port: parseInt(env.MYSQL_PORT ?? '3306'),
    username: env.MYSQL_USER,
    password: env.MYSQL_PASSWORD,
    database: env.MYSQL_DATABASE,
    entities: [path.join(__dirname, '**', '*.entity.ts')],
    synchronize: false,
    autoLoadEntities: true
  })
  return {
    type: 'mysql',
    host: env.MYSQL_HOST,
    port: parseInt(env.MYSQL_PORT ?? '3306'),
    username: env.MYSQL_USERNAME,
    password: env.MYSQL_PASSWORD,
    database: env.MYSQL_DATABASE,
    entities: [path.join(__dirname, '**', '*.entity.ts')],
    synchronize: false,
    autoLoadEntities: true
  }
}
