import helmet from 'helmet'
import cors from 'cors'
import path from 'path'
import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { StatusModule } from '@modules/status/status.module'
import { ConfigMiddleware } from '@middlewares/config.middleware'
import { TypeOrmModule } from '@nestjs/typeorm'
import { LoggerModule } from '@logger/logger.module'
import { SessionModule } from '@modules/session/session.module'
import { UserModule } from '@modules/user/user.module'
import AuthMiddleware from '@middlewares/auth.middleware'
import { AuthenticationModule } from '@modules/authentication/authentication.module'
import { GenderModule } from '@modules/gender/gender.module'
import { ProfileModule } from '@modules/profile/profile.module'
import { ContextModule } from '@modules/context/context.module'
import { TagModule } from '@modules/tag/tag.module'
import { MovementModule } from '@modules/movement/movement.module'
import { ProfileBalanceModule } from '@modules/profileBalance/profile-balance.module'
import { ProfileViewModule } from '@modules/profileView/profile-view.module'

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST,
      port: parseInt(process.env.MYSQL_PORT ?? '3306'),
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE,
      entities: [path.join(__dirname, '**', '*.entity.ts')],
      synchronize: false,
      autoLoadEntities: true,
      logging: false // ['query']
    }),
    LoggerModule,
    ContextModule,
    AuthenticationModule,
    UserModule,
    StatusModule,
    SessionModule,
    GenderModule,
    ProfileModule,
    TagModule,
    MovementModule,
    ProfileBalanceModule,
    ProfileViewModule
  ]
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(cors(), helmet(), ConfigMiddleware).forRoutes('*')
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'sessions/login', method: RequestMethod.POST },
        { path: 'users', method: RequestMethod.POST },
        { path: 'status', method: RequestMethod.ALL },
        { path: 'genders', method: RequestMethod.ALL }
      )
      .forRoutes('*')
  }
}
