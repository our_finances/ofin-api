import { Injectable, Scope } from '@nestjs/common'
import * as winston from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import { contextKeys, getContext } from '@utils/context.util'
import { format } from 'date-fns'
import { NullOrUndefined } from '@typings/generic.typing'
import { LoggerAbstract } from './logger.abstract'

const { combine, timestamp, printf, colorize } = winston.format

const colors = {
  trace: 'magenta',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  debug: 'blue',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  error: 'red',
  yellow: 'yellow',
  blue: 'blue',
  green: 'green',
  red: 'red',
  grey: 'grey',
  gray: 'gray',
  cyan: 'cyan',
  black: 'black',
  white: 'white',
  magenta: 'magenta'
}

@Injectable({ scope: Scope.TRANSIENT })
export class WinstonLoggerService extends LoggerAbstract {
  private readonly logger: winston.Logger
  private context: NullOrUndefined<string> = null

  constructor() {
    super()
    this.logger = winston.createLogger({
      transports: [
        new DailyRotateFile({
          filename: './logs/file-%DATE%.log',
          datePattern: 'DD_MM_yyyy',
          maxFiles: '14d',
          format: combine(timestamp(), this.fileFormat())
        }),
        new winston.transports.Console({
          level: process.env.LOGGER_LEVEL || 'debug',
          format: combine(timestamp(), this.consoleFormat())
        })
      ],
      exitOnError: false
    })
  }

  warn(message: any, ...optionalParams: any[]) {
    throw new Error('Method not implemented.')
  }

  private getLoggerContext(key: keyof contextKeys['req']): string {
    const values = getContext('req')
    return values ? values[key] : '-'
  }

  private consoleFormat() {
    return printf(({ level, message, timestamp }) => {
      const tempLevel = level as keyof typeof colors
      const requestId = this.getLoggerContext('requestId')
      const method = this.getLoggerContext('method')
      const originalUrl = this.getLoggerContext('originalUrl')
      const context = this.context || 'Main'
      const formatTime = format(new Date(timestamp), 'dd/MM/yyyy HH:mm:ss')
      const buildMessage: string[] = []
      buildMessage.push(this.c('grey', formatTime))
      buildMessage.push(`[ ${this.c(tempLevel, tempLevel)} ]`)
      buildMessage.push(`[ ${this.c('magenta', requestId)} ]`)
      buildMessage.push(this.c('white', `[ (${method}) ${originalUrl} ]`))
      buildMessage.push(`[ ${this.c('yellow', context)} ]`)
      buildMessage.push('::')
      buildMessage.push(`${this.c(tempLevel, message)}`)
      return buildMessage.join(' ')
    })
  }

  private c(color: keyof typeof colors, message: string): string {
    const colorizer = colorize({ colors }).colorize
    return colorizer(color, message)
  }

  private fileFormat() {
    return printf(({ level, message, timestamp }) => {
      return JSON.stringify({
        timestamp,
        level,
        id: this.getLoggerContext('requestId'),
        method: this.getLoggerContext('method'),
        url: this.getLoggerContext('originalUrl'),
        message
      })
    })
  }

  debug(message: any, ...optionalParam: any[]) {
    this.logger.debug(this.buildWithOptionalParam(message, optionalParam))
  }

  verbose(message: any, ...optionalParam: any[]) {
    this.logger.verbose(this.buildWithOptionalParam(message, optionalParam))
  }

  info(message: any, ...optionalParam: any[]) {
    this.logger.info(this.buildWithOptionalParam(message, optionalParam))
  }

  error(message: any, ...optionalParam: any[]) {
    this.logger.error(this.buildWithOptionalParam(message, optionalParam))
  }

  log(msg: any) {
    this.info(msg)
  }

  setContext(context: string) {
    this.context = context
  }
}
