import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import * as httpContext from 'express-http-context'
import { WinstonLoggerService } from '@logger/winston-logger.service'
import { AllExceptionsFilter } from '@exceptions/all.exception'
import { envConfig } from '@config/env.config'
import express from 'express'

const logger = new WinstonLoggerService()

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger
  })
  app.use(express.json())
  app.use(httpContext.middleware)
  app.useGlobalFilters(new AllExceptionsFilter())
  await app.listen(envConfig.port)
}

bootstrap().then(() => {
  logger.log(`Server initialized in port: ${envConfig.port}`)
})
